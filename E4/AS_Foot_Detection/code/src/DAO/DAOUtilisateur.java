package DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import classe.Categorie;
import classe.Joueurs;
import classe.Utilisateur;
import classe.Bulletin;

public class DAOUtilisateur {
	
	/**
	 * methode qui permet de se connecter a la base de donn�es qui prend rien en parametre
	 * @return conn
	 * 
	 */
	 private Connection getConnexion(){
		 System.out.println("modification realis�es: ");
	        Connection conn = null ;
	        String driver = "com.mysql.cj.jdbc.Driver"; //TODO � compl�ter
	        String url = "jdbc:mysql://mysql-yonhisio.alwaysdata.net/yonhisio_as_foot_detection"; //TODO � compl�ter
	        String user="yonhisio"; //TODO � compl�ter
	        String pwd = "93eGKwAbm9EVHrm"; //TODO � compl�ter
	        try{
	            Class.forName(driver);
	            System.out.println("driver ok");
	            conn=	DriverManager.getConnection(url,user,pwd);
	            System.out.println("connection ok");

	        }catch(Exception e){
	            e.printStackTrace();
	        }
	        return conn;
	    }
	 
	 /**
		 * methode qui permet d'ajouter un utilisateur dans la bdd
		 * @param utl un utilisateur
		 */
	 public void insertUtilisateur(Utilisateur utl ) 
	    {
		 Connection conn = null;
	        
	        // pr�parer une requ�te d'insertion
	        // remplir la requ�te avec les donn�es de l'objet pass� en param�tre (p)
	        // ex�cuter la requ�te
	    	
		 try{
			 conn = getConnexion();
				
				//cr�ation de la requete pr�par�e
				String req = "INSERT INTO utilisateur VALUES(?,?,?,?)";
				PreparedStatement pstmt = conn.prepareStatement(req);
				
				//pr�paration de la requete
				
				pstmt.setString(1, utl.getLogin());
				pstmt.setString(2, utl.getEmail());
				pstmt.setString(3, utl.getMdp());
				pstmt.setString(4, utl.getType());
			
				
				
				//execution de la requete pr�par�e de mise a jour  (DELETET,INSERT,UDATE)
				int res2 = pstmt.executeUpdate();
				System.out.println(res2+ " utilisateur � �t� ajout�");
				
				
		 }catch(Exception e){
				e.printStackTrace();
				}
		 finally{
			    try{
			        if (conn!=null){
			            conn.close();
			        }
			    } catch (SQLException e){
			        e.printStackTrace();
			    }
			}
	    	}
	 
	 
	 
	 /**
		 * methode qui permet de recuperer un utilisateur par son nom
		 * @param nom une chaine de carractere
		 * @return un utilisateur
		 * 
		 */
	 public Utilisateur getUserByName(String nom) {
		 Utilisateur utl  = null;
		 Connection conn = null;
		 try{
			 
			 conn = getConnexion();
				
				//cration de la requete prpare
				String req = "SELECT* FROM utilisateur WHERE id = ?";
				PreparedStatement pstmt = conn.prepareStatement(req);
				//preparation de la requete
				pstmt.setString(1,nom);
				//execution de la requete
				ResultSet result = pstmt.executeQuery();
				
				while(result.next()){
	                utl= new Utilisateur();
	                
	                utl.setLogin(result.getString(1));
	                utl.setEmail(result.getString(2));
	                utl.setMdp(result.getString(3));
	                utl.setType(result.getString(4));
	            
	                }
					
						
		 }
		 catch(Exception e){
				e.printStackTrace();
				}
		 finally{
			    try{
			        if (conn!=null){
			            conn.close();
			        }
			    } catch (SQLException e){
			        e.printStackTrace();
			    }
			}
		
		 return utl;
	 }
	 
	 
	 /**
		 * methode qui permet de se connecter a l'application
		 * @param login , mdp des chaines de carracteres
		 * @return une chaine de carratere
		 * 
		 */
	 public String verifUser(String login,String mdp) {
		 Utilisateur utl  = null;
		 Connection conn =  null;
		 try{
			 
			 conn = getConnexion();
				
			 String req = "SELECT * FROM utilisateur WHERE login = ? and mdp= ? ";
				PreparedStatement pstmt = conn.prepareStatement(req);
				pstmt.setString(1,login);
				pstmt.setString(2,mdp);
				ResultSet result = pstmt.executeQuery();
				
				
				
				
				
				if(result.next()){
					System.out.println("vous etes connect�");
					
				
				}
				else {
					System.out.println("Le login ou ou mot de passe est faux");
				}
				
				String loginbdd = result.getString(1);
				String email = result.getString(2);
				String mdpbdd = result.getString(3);
				String type = result.getString(4);
				
				utl  = new Utilisateur (loginbdd,email,mdpbdd,type);
				String userType = utl.getType();
				
				
						
		 }
		 catch(Exception e){
				e.printStackTrace();
				}
		 finally{
			    try{
			        if (conn!=null){
			            conn.close();
			        }
			    } catch (SQLException e){
			        e.printStackTrace();
			    }
			}
		
		 return utl.getType();
	 }
	 
	 
	 
	 /**
		 * methode qui modifie le mot de passe de l'utilisateur
		 * @param un login saisie par l'utilisateur et de type String
		 * @return res de type int corespondant a la modification realis�e, 0 ou 1 
		 * 
		 */
	 public int updatePwd(String login,String newMdp) 
	    {
		 Connection conn = null;
		 int res = 0;
	        
	        // pr�parer une requ�te d'insertion
	        // remplir la requ�te avec les donn�es de l'objet pass� en param�tre (p)
	        // ex�cuter la requ�te
	    	
		 try{
			 conn = getConnexion();
				
				//cr�ation de la requete pr�par�e
				String req = "UPDATE utilisateur SET mdp = ? WHERE login = ?" ;
				PreparedStatement pstmt = conn.prepareStatement(req);
				
				//pr�paration de la requete
				
				pstmt.setString(1,newMdp);
				pstmt.setString(2,login);
				
				//execution de la requete pr�par�e de mise a jour  (DELETET,INSERT,UDATE)
				res = pstmt.executeUpdate();
				System.out.println(res+ " mot de passe modifi�");
					
				
		 }catch(Exception e){
				e.printStackTrace();
				}
		 finally{
			    try{
			        if (conn!=null){
			            conn.close();
			        }
			    } catch (SQLException e){
			        e.printStackTrace();
			    }
			}
		 
		 return res;
	    	}

}



