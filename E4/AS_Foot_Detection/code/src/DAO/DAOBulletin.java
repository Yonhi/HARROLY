package DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import classe.Categorie;
import classe.Joueurs;
import classe.Bulletin;

public class DAOBulletin {

	/**
	 * methode qui permet de se connecter a la base de donn�es qui prend rien en parametre
	 * @return conn
	 * 
	 */
	private Connection getConnexion(){
		System.out.println("modification realis�es: ");
		Connection conn = null ;
		String driver = "com.mysql.cj.jdbc.Driver"; //TODO � compl�ter
		String url = "jdbc:mysql://mysql-yonhisio.alwaysdata.net/yonhisio_as_foot_detection"; //TODO � compl�ter
		String user="yonhisio"; //TODO � compl�ter
		String pwd = "93eGKwAbm9EVHrm"; //TODO � compl�ter
		try{
			Class.forName(driver);
			System.out.println("driver ok");
			conn=	DriverManager.getConnection(url,user,pwd);
			System.out.println("connection ok");

		}catch(Exception e){
			e.printStackTrace();
		}
		return conn;
	}


	public void insertBulletin(Bulletin bltn,Joueurs j ) 
	{
		Connection conn = null;
		int nbBulletin = 0;

		// pr�parer une requ�te d'insertion
		// remplir la requ�te avec les donn�es de l'objet pass� en param�tre (p)
		// ex�cuter la requ�te

		try{
			conn = getConnexion();

			//cr�ation de la requete pr�par�e
			Statement stmt = conn.createStatement();
			String reqa = "SELECT (COUNT(*)) from bulletin_de_note";
			ResultSet result = stmt.executeQuery(reqa);

			//pr�paration de la requete
			while(result.next()){
				nbBulletin = result.getInt(1);	
				



			}
			result.close();
			stmt.close();



	
			
			

			//cr�ation de la requete pr�par�e
			String req = "INSERT INTO bulletin_de_note VALUES(?,?,?,?,?,?,?)";
			PreparedStatement pstmt = conn.prepareStatement(req);

			//pr�paration de la requete
			pstmt.setInt(1,nbBulletin+1);
			pstmt.setString(2, bltn.getFrancais());
			pstmt.setString(3, bltn.getMaths());
			pstmt.setString(4, bltn.getHistGeo());
			pstmt.setString(5, bltn.getAnglais());
			pstmt.setString(6, bltn.getTrimestre());
			pstmt.setString(7,j.getId());


			//execution de la requete pr�par�e de mise a jour  (DELETET,INSERT,UDATE)
			int res2 = pstmt.executeUpdate();
			System.out.println("modification realis�es: "+res2);


		}catch(Exception e){
			e.printStackTrace();
		}
		finally{
			try{
				if (conn!=null){
					conn.close();
				}
			} catch (SQLException e){
				e.printStackTrace();
			}
		}
	}

}



