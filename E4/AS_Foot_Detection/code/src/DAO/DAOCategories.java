package DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


import classe.Categorie;





public class DAOCategories {
	

	/**
	 * methode qui permet de se connecter a la base de donn�es qui prend rien en parametre
	 * @return conn
	 * 
	 */
	private Connection getConnexion(){
		 
	        Connection conn = null ;
	        String driver = "com.mysql.cj.jdbc.Driver"; //TODO � compl�ter
	        String url = "jdbc:mysql://mysql-yonhisio.alwaysdata.net/yonhisio_as_foot_detection"; //TODO � compl�ter
	        String user="yonhisio"; //TODO � compl�ter
	        String pwd = "93eGKwAbm9EVHrm"; //TODO � compl�ter
	        try{
	            Class.forName(driver);
	            System.out.println("driver ok");
	            conn=	DriverManager.getConnection(url,user,pwd);
	            System.out.println("connection ok");

	        }catch(Exception e){
	            e.printStackTrace();
	        }
	        return conn;
	    }
	 
	 
	
	
	 
	 /**
		 * methode qui permet de recuperer une categorie existant dans la bdd
		 * @param libelle : le libell� saisie en parametre pour r�cup�r� toutes ses informations 
		 * @return cat qui coresspond � la categorie r�cup�r� de type Categorie
		 * 
		 */
	 
	 public Categorie getCategorieByLibelle(String libelle) {
		 
		 Connection conn = null;
		 Categorie cat = null;
		 try{
			 
			 conn = getConnexion();
				
				//cration de la requete prpare
				String req = "SELECT* FROM categories WHERE libelle = ?";
				PreparedStatement pstmt = conn.prepareStatement(req);
				//preparation de la requete
				pstmt.setString(1,libelle);
				//execution de la requete
				ResultSet result = pstmt.executeQuery();
				
				while(result.next()){
	               cat = new Categorie();
	               cat.setId(result.getString(1));
	               cat.setLibelle(result.getString(2));
	              
	                }
				 
					
				
		 }
		 catch(Exception e){
				e.printStackTrace();
				}
		 finally{
			    try{
			        if (conn!=null){
			            conn.close();
			        }
			    } catch (SQLException e){
			        e.printStackTrace();
			    }
			}
		
		 return cat;
	 }
	 
	 
	 /**
		 * methode qui permet d'afficher les categories
		 * @return conn
		 * 
		 */
	 public void afficherCategories()  {
		 Connection conn = null;
		try{
			conn = getConnexion();
			
			//affiche le contenue de la table avec SELECT
			Statement stmt = conn.createStatement();
			String req= "SELECT* FROM categories";
			ResultSet result = stmt.executeQuery(req);
			
			// On affiche les donnes des diffrentes colonnes
			while(result.next()){
			System.out.print( "id: "+ result.getString(1));	
			System.out.println( "nom : "+ result.getString(2));
			
			
			
			}
			result.close();
			stmt.close();
			
			} catch(Exception e){
				e.printStackTrace();
				}
			 finally{
				    try{
				        if (conn!=null){
				            conn.close();
				        }
				    } catch (SQLException e){
				        e.printStackTrace();
				    }
				}
		}
	 
	 
	 
	 /**
		 * methode qui permet d'afficher les infos d'une categorie
		 * @param cat de Type categorie
		 * 
		 */
	 public void afficherCategoriesBycat(Categorie cat)  {
		 Connection conn = null;
		 
		try{
			conn = getConnexion();
		
			
			//affiche le contenue de la table avec SELECT
			
			String req= "SELECT* FROM categories where id = ?";
			PreparedStatement pstmt = conn.prepareStatement(req);
			pstmt.setString(1,cat.getId());
			ResultSet result = pstmt.executeQuery(req);
			
			// On affiche les donnes des diffrentes colonnes
			while(result.next()){
			System.out.print( "id: "+ result.getString(1));	
			System.out.println( "nom : "+ result.getString(2));
			
			
			
			}
			result.close();
			pstmt.close();
			
			} catch(Exception e){
				e.printStackTrace();
				}
			 finally{
				    try{
				        if (conn!=null){
				            conn.close();
				        }
				    } catch (SQLException e){
				        e.printStackTrace();
				    }
				}
		}
	 
	 
	 /**
		 * methode qui recupere toutes les categorie de la bdd 
		 * @return une liste de categorie de type Arrayliste 
		 * 
		 */
	 public ArrayList<String>getAllCategories(){
			
		 ArrayList<String> listC = new ArrayList();
		 String cat =  null ; 
		 Connection conn = null;
		 try{
			 conn = getConnexion();
				
				//affiche le contenue de la table avec SELECT
				Statement stmt = conn.createStatement();
				String req2= "SELECT* FROM categories";
				ResultSet result = stmt.executeQuery(req2);
		
				// On affiche les donnes des diffrentes colonnes
				while(result.next()){
					cat = result.getString(2);
					listC.add(cat);
					}
				 
				
		 }  
		 catch(Exception e){
				e.printStackTrace();
				}
		 finally{
			    try{
			        if (conn!=null){
			            conn.close();
			        }
			    } catch (SQLException e){
			        e.printStackTrace();
			    }
			}
		 return listC;
	}
	 
	 
	 /**
		 * methode qui recupere l'id de la categorie de la bdd
		 * @return l'id de la categorie du type String
		 * 
		 */
	 public Categorie getCategorieId(String libelleCat) {
		 Categorie cat = null;
		 Connection conn = null;
		 try{
			 
			 conn = getConnexion();
				
				//creation de la requete prepare
				String req = "SELECT* FROM categories WHERE id = ?";
				PreparedStatement pstmt = conn.prepareStatement(req);
				//preparation de la requete
				pstmt.setString(1,libelleCat);
				//execution de la requete
				ResultSet result = pstmt.executeQuery();
				
				while(result.next()){
	                
					cat = new Categorie();
		               cat.setId(result.getString(1));
		               cat.setLibelle(result.getString(2));
	                
	                }
				
					
						
		 }catch (SQLException e){
		        e.printStackTrace();
		    }
		 
		 finally{
			    try{
			        if (conn!=null){
			            conn.close();
			        }
			    } catch (SQLException e){
			        e.printStackTrace();
			    }
			}
		
		 return cat ;
	 }
	 
	
	 

}
