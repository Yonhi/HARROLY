package DAO;

import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;
import classe.Recruteur;
import classe.Categorie;
import classe.Utilisateur;


public class DAORecruteur {

	
	/**
	 * methode qui permet de se connecter a la base de donn�es qui prend rien en parametre
	 * @return conn
	 * 
	 */
	private Connection getConnexion(){
		 System.out.println("modification realis�es: ");
	        Connection conn = null ;
	        String driver = "com.mysql.cj.jdbc.Driver"; //TODO � compl�ter
	        String url = "jdbc:mysql://mysql-yonhisio.alwaysdata.net/yonhisio_as_foot_detection"; //TODO � compl�ter
	        String user="yonhisio"; //TODO � compl�ter
	        String pwd = "93eGKwAbm9EVHrm"; //TODO � compl�ter
	        try{
	            Class.forName(driver);
	            System.out.println("driver ok");
	            conn=	DriverManager.getConnection(url,user,pwd);
	            System.out.println("connection ok");

	        }catch(Exception e){
	            e.printStackTrace();
	        }
	        return conn;
	    }
	
	
	/**
	 * methode qui permet d'inserer un Recruteur dans la bdd
	 * @param le recruteur de type Recruteur et une categorie deja existante dans la bdd de type Categorie
	 * 
	 */
	 public int insertRecruteur(Recruteur rct, Categorie cat ) 
	    {
		 Connection conn = null;
		 int res2 = 0;
	        
	        // pr�parer une requ�te d'insertion
	        // remplir la requ�te avec les donn�es de l'objet pass� en param�tre (p)
	        // ex�cuter la requ�te
	    	
		 try{
			 conn = getConnexion();
				
				//cr�ation de la requete pr�par�e
				String req = "INSERT INTO recruteurs VALUES(?,?,?)";
				PreparedStatement pstmt = conn.prepareStatement(req);
				
				//pr�paration de la requete
				
				pstmt.setString(1, rct.getId());
				pstmt.setString(2, rct.getClub());
				pstmt.setString(3, cat.getId());
			
				
				
				//execution de la requete pr�par�e de mise a jour  (DELETET,INSERT,UDATE)
				res2 = pstmt.executeUpdate();
				System.out.println(res2+ " recruteur a �t� ajout�");
				
				
		 }catch(Exception e){
				e.printStackTrace();
				}
		 finally{
			    try{
			        if (conn!=null){
			            conn.close();
			        }
			    } catch (SQLException e){
			        e.printStackTrace();
			    }
			}
		 
		 return res2;
	    	}
	 
	 
	 /**
		 * methode qui suprime un joueur selon son login
		 * @param un login saisie par l'utilisateur et de type String
		 * @return res3 de type int corespondant a la modification realis�e, 0 ou 1 
		 * 
		 */
	 public int deleteUser(String rct) 
	    {
		 Connection conn = null;
		 int res3 = 0;
	        
	        // pr�parer une requ�te d'insertion
	        // remplir la requ�te avec les donn�es de l'objet pass� en param�tre (p)
	        // ex�cuter la requ�te
	    	
		 try{
			 conn = getConnexion();
				
				//cr�ation de la requete pr�par�e
				String req = "DELETE FROM recruteurs where id =  ?";
				PreparedStatement pstmt = conn.prepareStatement(req);
				
				//pr�paration de la requete
				
				pstmt.setString(1,rct);
			
				
				
				//execution de la requete pr�par�e de mise a jour  (DELETET,INSERT,UDATE)
				int res2 = pstmt.executeUpdate();
				System.out.println(res2+ " Recruteur suprim�");
				
				
				
				//cr�ation de la requete pr�par�e
				String req2 = "DELETE FROM utilisateur where login =  ?";
				PreparedStatement pstmt2 = conn.prepareStatement(req2);
				
				//pr�paration de la requete
				
				pstmt2.setString(1,rct);
			
				
				
				//execution de la requete pr�par�e de mise a jour  (DELETET,INSERT,UDATE)
				res3 = pstmt2.executeUpdate();
				System.out.println(res3+ " Utilisateur suprim�");
				
				
				
				
		 }catch(Exception e){
				e.printStackTrace();
				}
		 finally{
			    try{
			        if (conn!=null){
			            conn.close();
			        }
			    } catch (SQLException e){
			        e.printStackTrace();
			    }
			}
		 
		 return res3;
	    	}
	 
	 
	 

	 
	 
	 
	 
	
}
