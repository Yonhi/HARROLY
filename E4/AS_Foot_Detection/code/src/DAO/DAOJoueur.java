package DAO;

import java.sql.Connection;
import classe.Utilisateur;

import classe.Joueurs;
import DAO.*;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import classe.Categorie;
import classe.Joueurs;

public class DAOJoueur {
	DAOCategories daoC = new DAOCategories();
	
	/**
	 * methode qui permet de se connecter a la base de donn�es qui prend rien en parametre
	 * @return conn
	 * 
	 */
	private Connection getConnexion(){
		 System.out.println("modification realis�es: ");
	        Connection conn = null ;
	        String driver = "com.mysql.cj.jdbc.Driver"; //TODO � compl�ter
	        String url = "jdbc:mysql://mysql-yonhisio.alwaysdata.net/yonhisio_as_foot_detection"; //TODO � compl�ter
	        String user="yonhisio"; //TODO � compl�ter
	        String pwd = "93eGKwAbm9EVHrm"; //TODO � compl�ter
	        try{
	            Class.forName(driver);
	            System.out.println("driver ok");
	            conn=	DriverManager.getConnection(url,user,pwd);
	            System.out.println("connection ok");

	        }catch(Exception e){
	            e.printStackTrace();
	        }
	        return conn;
	    }
	 
	/**
	 * methode qui permet d'ajouter un joueur dans la bdd
	 * @param j un Joueur, cat une categorie, utl un utilisateur
	 * 
	 * 
	 */
	 public int insertJoueur(	Joueurs j,Categorie cat,Utilisateur utl) 
	    {
		 Connection conn = null;
		 int res2 = 0;
	        
	        // pr�parer une requ�te d'insertion
	        // remplir la requ�te avec les donn�es de l'objet pass� en param�tre (p)
	        // ex�cuter la requ�te
	    	
		 try{
			 conn = getConnexion();
				
				//cr�ation de la requete pr�par�e
				String req = "INSERT INTO joueurs VALUES(?,?,?,?,?,?,?,?,?)";
				PreparedStatement pstmt = conn.prepareStatement(req);
				
				//pr�paration de la requete
				pstmt.setString(1,utl.getLogin());
				pstmt.setString(2,j.getPrenom());
				pstmt.setString(3,j.getNom());
				pstmt.setString(4,j.getPoste());
				pstmt.setString(5,j.getClub());
				pstmt.setString(6,j.getPoids());
				pstmt.setString(7,cat.getId());
				pstmt.setString(8,j.getNbButs());
				pstmt.setString(9,j.getNbPasseD());
				
				
				
				
				//execution de la requete pr�par�e de mise a jour  (DELETET,INSERT,UDATE)
				 res2 = pstmt.executeUpdate();
				System.out.println("modification realis�es: "+res2);
				
				
		 }catch(Exception e){
				e.printStackTrace();
				}
		 finally{
			    try{
			        if (conn!=null){
			            conn.close();
			        }
			    } catch (SQLException e){
			        e.printStackTrace();
			    }
			}
		 return res2;
	    	}
	 
	 /**
		 * methode qui permet d'afficher les infos d'un joueur
		 * @param cat de Type Joueurs
		 * 
		 */
public ArrayList<Joueurs> getJoueurByPost(String libelle) {
		 
		 Connection conn = null;
		 Categorie cat = null;
		 ArrayList<Joueurs>listJ = new ArrayList<Joueurs>();
		 DAOCategories daoC = new DAOCategories();
		 try{
			 
			 conn = getConnexion();
				
				//cration de la requete prpare
				String req = "select * from joueurs,categories where joueurs.categorie = categories.id and poste = ?";
				PreparedStatement pstmt = conn.prepareStatement(req);
				//preparation de la requete
				pstmt.setString(1,libelle);
				//execution de la requete
				ResultSet result = pstmt.executeQuery();
				
				while(result.next()){
					String id = result.getString(1);
					String nom =  result.getString(2);
					String prenom = result.getString(3);
					String poste = result.getString(4);
					String club = result.getString(5);
					String poids = result.getString(6);
					
					String but = result.getString(8);
					String passe = result.getString(9);
					
					
					cat = daoC.getCategorieByLibelle(result.getString(7));
					Joueurs j = new Joueurs(id, nom, prenom, poste, club, poids, cat, but, passe); 
					listJ.add(j);
					
					
					
					
					
					
	              
	                }
				 
					
				
		 }
		 catch(Exception e){
				e.printStackTrace();
				}
		 finally{
			    try{
			        if (conn!=null){
			            conn.close();
			        }
			    } catch (SQLException e){
			        e.printStackTrace();
			    }
			}
		 return listJ;
		
		
	 }

/**
 * methode qui permet de recuperer un joueur selon son login
 * @param id un entier, le login
 * @return un joueur de type Joueur
 * 
 */
 public Joueurs getJoueurById(String id) {
	 Joueurs j  = null;
	 Connection conn = null;
	 
	 
	 
	 
	 try{
		 
		 conn = getConnexion();
			
			//cration de la requete prpare
			String req = "SELECT* FROM joueurs where id = ?";
			PreparedStatement pstmt = conn.prepareStatement(req);
			//preparation de la requete
			pstmt.setString(1,id);
			//execution de la requete
			ResultSet result = pstmt.executeQuery();
			
			while(result.next()){
                j = new Joueurs();
                Categorie cat = new Categorie();
                j.setId(result.getString(1));
                j.setNom(result.getString(2));
                j.setPrenom(result.getString(3));
                j.setPoste(result.getString(4));
                j.setClub(result.getString(5));
                j.setPoids(result.getString(6));
                
                cat = daoC.getCategorieId(result.getString(7));
               
                j.setCategorie(cat);
                
                j.setNbButs(result.getString(8));
                j.setNbPasseD(result.getString(9));
               
                }
               
				
					
	 }catch(Exception e){
			e.printStackTrace();
			}
	 
	 finally{
		    try{
		        if (conn!=null){
		            conn.close();
		        }
		    } catch (SQLException e){
		        e.printStackTrace();
		    }
		}
	 return j;
 }

}


