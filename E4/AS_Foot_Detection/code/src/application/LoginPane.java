package application;



import DAO.DAORecruteur;
import DAO.DAOUtilisateur;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;




public class LoginPane extends GridPane {
	public LoginPane() {


		this.setAlignment(Pos.TOP_CENTER);// le this sert a dire que c'est le grid de cette page
		this.setHgap(10);
		this.setVgap(10);
		this.setPadding(new Insets(25, 25, 25, 25));

		/*
		 * forme de l'interface
		 */
		
		
		Text scenetitle = new Text("AS FOOT DETECTION  ");
		scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 70));
		this.add(scenetitle,0, 0, 2, 1);

		Text connect = new Text("Se connecter ");
		connect.setFont(Font.font("Arial", FontWeight.NORMAL, 30)); 
		this.add(connect, 0,1);

		//champs texte login et mdp 
		Label labelLogin = new Label("Login");
		this.add(labelLogin, 0, 5);
		
		TextField login= new TextField();
		this.add(login, 1, 5);
		login.setMaxSize(300, 300);
		VBox loginV = new VBox();

		Label labelMdp = new Label("Mot de passe");
		this.add(labelMdp, 0, 7);
		
		PasswordField mdp= new PasswordField();
		this.add(mdp, 1, 7);
		mdp.setMaxSize(300, 300);

		//bouton valider et s'incrire
		Button btnValide = new Button("Valider");
		this.add (btnValide, 1, 11 );
		btnValide.setMaxSize(150, 150);
		btnValide.setAlignment(Pos.CENTER);

		Button btnIns = new Button("Cree un compte");
		btnIns.setMaxSize(150, 150);
		btnIns.setAlignment(Pos.CENTER);
		this.add (btnIns, 1, 12 );






		//gere les evenement 
		btnValide.setOnAction(new EventHandler<ActionEvent>()
		{
			@Override public void handle(ActionEvent e)
			{
				try {

					DAOUtilisateur dao = new DAOUtilisateur ();
					dao.verifUser(login.getText(), mdp.getText());
					if (dao.verifUser(login.getText(), mdp.getText()).equals("joueur")) {
						System.out.println("page joueur");
						Main.getScene().setRoot((Parent) Main.getPanel("JoueurPane"));
						
						DAORecruteur dao1 = new DAORecruteur();
					
						
					}
					else if (dao.verifUser(login.getText(), mdp.getText()).equals("recruteur")) {
						System.out.println("page recruteur");
						Main.getScene().setRoot((Parent) Main.getPanel("RecruteurPane"));
						
						DAORecruteur dao1 = new DAORecruteur();
					
						
					}


				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}





			}
		});
		
		btnIns.setOnAction(new EventHandler<ActionEvent>()
		{
			@Override public void handle(ActionEvent e)
			{
				try {

					Main.getScene().setRoot((Parent) Main.getPanel("InscriptionPane"));

				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}





			}
		});
		
		
	}


}

