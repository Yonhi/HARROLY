package application;



import DAO.DAOUtilisateur;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;




public class InscriptionPane extends GridPane {
	public InscriptionPane() {


		this.setAlignment(Pos.TOP_CENTER);
		this.setHgap(10);
		this.setVgap(10);
		this.setPadding(new Insets(25, 25, 25, 25));

		/*
		 * forme de l'interface
		 */
		Text scenetitle = new Text("AS FOOT  ");
		scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 100));
		this.add(scenetitle,1, 0, 2, 1);

		Text connect = new Text("Etes vous un joueur ou un recruteur ? ");
		connect.setFont(Font.font("Arial", FontWeight.NORMAL, 20)); 
		this.add(connect, 1,1,1, 1);

		
		
		
		//bouton valider et s'incrire
		Button btnJoueur = new Button("Joueur");
		HBox hBtnJoueur = new HBox (10);
		hBtnJoueur.getChildren (). add (btnJoueur);
		this.add (hBtnJoueur, 1, 5);
		
		Button btnRecruteur = new Button("Recruteur");
		HBox hBtnRecteur = new HBox (300);
		hBtnRecteur.getChildren (). add (btnRecruteur);
		this.add (hBtnRecteur, 1, 6);

		
		Button retour = new Button("Retour");
		HBox hBtnRetour = new HBox (10);
		hBtnRetour.getChildren (). add (retour);
		this.add (hBtnRetour, 	0, 0);





		//gere les evenement 
		btnJoueur.setOnAction(new EventHandler<ActionEvent>()
		{
			@Override public void handle(ActionEvent e)
			{
				try {
					Main.getScene().setRoot(Main.getPanel("InsJoueurPane"));

					


				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}





			}
		});
		
		
		
		btnRecruteur.setOnAction(new EventHandler<ActionEvent>()
		{
			@Override public void handle(ActionEvent e)
			{
				try {
					Main.getScene().setRoot(Main.getPanel("InsRecruteurPane"));
				

				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}





			}
		});
		
		retour.setOnAction(new EventHandler<ActionEvent>()
		{
			@Override public void handle(ActionEvent e)
			{
				try {
					Main.getScene().setRoot(Main.getPanel("LoginPane"));
				

				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}





			}
		});
		
		
	}


}

