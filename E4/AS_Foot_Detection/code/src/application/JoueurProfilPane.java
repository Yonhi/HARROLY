package application;





import DAO.DAOJoueur;
import DAO.DAORecruteur;
import DAO.DAOUtilisateur;
import classe.Joueurs;
import javafx.application.Application;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.ObservableList;
import javafx.stage.Stage;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;




public class JoueurProfilPane extends GridPane {
	public JoueurProfilPane() {




		this.setAlignment(Pos.TOP_CENTER);// le this sert a dire que c'est le grid de cette page
		this.setHgap(10);
		this.setVgap(10);
		this.setPadding(new Insets(25, 25, 25, 25));

		/*
		 * forme de l'interface
		 */
		Text scenetitle = new Text("AS FOOT DETECTION  ");
		scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 70));
		this.add(scenetitle,1, 0, 2, 1);



		//text : Mon profil
		Text bvn = new Text(" Mon profil ");
		bvn.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
		this.add(bvn,1, 1, 2, 1);

		Text txtMdp = new Text(" Changement de mot de passe ");
		txtMdp.setFont(Font.font("Tahoma", FontWeight.NORMAL, 30));
		this.add(txtMdp,1, 2, 2, 1);

		Label LabelLogin= new Label("Saisir votre login");
		this.add(LabelLogin, 1, 4);

		TextField login= new TextField();
		login.setMaxSize(100, 100);
		login.setPromptText("Login");
		this.add(login, 1, 5);

		Label lblNewPwd = new Label("Saisir votre nouveau mot de passe");
		this.add(lblNewPwd, 1, 7);

		TextField newPwd = new TextField();
		newPwd.setMaxSize(100, 100);
		newPwd.setPromptText("Nouveau mot de passe");
		this.add(newPwd, 1, 8);

		Button btnVldMdp = new Button("Confirmer");
		this.add (btnVldMdp, 1,9 );
		btnVldMdp.setAlignment(Pos.CENTER);


		Text lblSupp = new Text(" Supprimer votre compte ");
		lblSupp.setFont(Font.font("Tahoma", FontWeight.NORMAL, 30));
		this.add(lblSupp,1, 11);

		TextField txtSupp = new TextField();
		txtSupp.setMaxSize(100, 100);
		txtSupp.setPromptText("Saisir votre login");
		this.add(txtSupp, 1, 13);




		//bouton pour valider la supression
		Button btnSupp = new Button("Suprimer votre compte");
		this.add (btnSupp, 1,14 );
		btnSupp.setAlignment(Pos.CENTER);
		
		Button retour = new Button("Retour");
		this.add (retour, 0,0 );
		retour.setAlignment(Pos.CENTER);




		//gere les evenement 
		btnSupp.setOnAction(new EventHandler<ActionEvent>()
		{
			@Override public void handle(ActionEvent e)
			{
				try {
					//j'instancie un object DAO pour avoir acces a ma methode qui supprime un recruteur
					DAORecruteur daoR = new DAORecruteur();

					//condition qui renvoie vers la page LoginePane si le recruteur a et� suprrim�
					if(daoR.deleteUser(txtSupp.getText()) == 1) {
						Main.getScene().setRoot((Parent) Main.getPanel("LoginPane"));
					}

				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}
		});
		btnVldMdp.setOnAction(new EventHandler<ActionEvent>()
		{
			@Override public void handle(ActionEvent e)
			{
				try {
					//j'instancie un object DAO pour avoir acces a ma methode qui supprime un recruteur
					DAOUtilisateur daoU = new DAOUtilisateur();

					//condition qui renvoie vers la page LoginePane si le recruteur a et� suprrim�
					if(daoU.updatePwd(login.getText(),newPwd.getText()) == 1) {
						Main.getScene().setRoot((Parent) Main.getPanel("LoginPane"));
					}

				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}
		});
		
		
		retour.setOnAction(new EventHandler<ActionEvent>()
		{
			@Override public void handle(ActionEvent e)
			{
				try {
					Main.getScene().setRoot((Parent) Main.getPanel("JoueurPane"));

				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}
		});

		



	}


}


