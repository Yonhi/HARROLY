package application;



import java.util.ArrayList;

import DAO.DAOJoueur;
import DAO.DAORecruteur;
import DAO.DAOUtilisateur;
import classe.Categorie;
import classe.Joueurs;
import classe.MatchDetection;
import javafx.application.Application;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.stage.Stage;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;




public class RecruteurPane extends GridPane {
	public RecruteurPane() {




		this.setAlignment(Pos.TOP_CENTER);// le this sert a dire que c'est le grid de cette page
		this.setHgap(10);
		this.setVgap(10);
		this.setPadding(new Insets(25, 25, 25, 25));

		/*
		 * forme de l'interface
		 */
		Text scenetitle = new Text("AS FOOT DETECTION  ");
		scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 70));
		this.add(scenetitle,1, 0, 2, 1);



		//champs texte login et mdp 
		Label LabelId= new Label("Rechercher un joueurs selon login : ");
		this.add(LabelId, 1, 5);

		TextField id= new TextField();
		id.setMaxSize(100, 40);
		this.add(id, 2, 5);
		
		Label LabelPoste= new Label("Rechercher des joueurs selon son Poste : ");
		this.add(LabelPoste, 1, 8);

		ComboBox listPoste = new ComboBox(); 
		listPoste.getItems().setAll("Attaquant","Milieu","Defenseur","Gardien");
		this.add(listPoste, 2, 8);

		
		
		
		
		
		
		TableView<Joueurs> table = new TableView<>();
		table.setMinWidth(getMinWidth());
		table.setMinHeight(getMinHeight());
		

		TableColumn<Joueurs, String> login = new TableColumn<>("login");
		TableColumn<Joueurs, String> nom = new TableColumn<>("nom");
		TableColumn<Joueurs, String> prenom = new TableColumn<>("prenom");
		TableColumn<Joueurs, String> poste2= new TableColumn<>("poste");
		TableColumn<Joueurs, String> club = new TableColumn<>("club");
		TableColumn<Joueurs, Categorie> cat = new TableColumn<>("categorie");
		TableColumn<Joueurs, String> poids = new TableColumn<>("poids");
		TableColumn<Joueurs, String> but = new TableColumn<>("buts");
		TableColumn<Joueurs, String> passe = new TableColumn<>("passes");
		
		
		
		table.getColumns().addAll(login,nom,prenom,poste2,club,cat,poids,but,passe);
		
		
		
		this.add(table,2,10);
		
		


		//bouton valider et s'incrire
		Button btnValide = new Button("Valider");
		this.add (btnValide, 2,6 );
		btnValide.setAlignment(Pos.CENTER);
		
		Button Vld2 = new Button("Valider");
		this.add (Vld2, 2,9 );
		Vld2.setAlignment(Pos.CENTER);

		Button monProfil = new Button("Modifier mon profil");
		this.add (monProfil, 4,0 );
		monProfil.setAlignment(Pos.CENTER);
		
		Button retour = new Button("deconnexion");
		this.add (retour, 0,0 );
		retour.setAlignment(Pos.CENTER);


		Text affJoueur = new Text();
	    this.add(affJoueur, 3, 6);






		//gere les evenement 
		btnValide.setOnAction(new EventHandler<ActionEvent>()
		{
			@Override public void handle(ActionEvent e)
			{
				try {
					DAOJoueur daoJ = new DAOJoueur();
					daoJ.getJoueurById(id.getText());
					Joueurs j = daoJ.getJoueurById(id.getText());
					System.out.println(j);
					
					affJoueur.setText(j.toString());
					




				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}





			}
		});
		
		Vld2.setOnAction(new EventHandler<ActionEvent>()
		{
			@Override public void handle(ActionEvent e)
			{
				try {
					DAOJoueur daoR = new DAOJoueur();
					daoR.getJoueurByPost(listPoste.getValue().toString());
				    ObservableList<Joueurs> oList = FXCollections.observableList(daoR.getJoueurByPost(listPoste.getValue().toString()));
					
					login.setCellValueFactory(new PropertyValueFactory<Joueurs,String>("login"));
					nom.setCellValueFactory(new PropertyValueFactory<Joueurs,String>("nom"));
				    prenom.setCellValueFactory(new PropertyValueFactory<Joueurs,String>("prenom"));
				    poste2.setCellValueFactory(new PropertyValueFactory<Joueurs,String>("poste"));
				    club.setCellValueFactory(new PropertyValueFactory<Joueurs,String>("club"));
				    cat.setCellValueFactory(new PropertyValueFactory<Joueurs,Categorie>("categorie"));
				    poids.setCellValueFactory(new PropertyValueFactory<Joueurs,String>("poids"));
				    but.setCellValueFactory(new PropertyValueFactory<Joueurs,String>("buts"));
				    passe.setCellValueFactory(new PropertyValueFactory<Joueurs,String>("passe"));
				    
				    table.getItems().addAll(oList);
					


				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}
		});



		monProfil.setOnAction(new EventHandler<ActionEvent>()
		{
			@Override public void handle(ActionEvent e)
			{
				try {
					Main.getScene().setRoot((Parent) Main.getPanel("RecruteurProfilPane"));




				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}
		});
		
		
		retour.setOnAction(new EventHandler<ActionEvent>()
		{
			@Override public void handle(ActionEvent e)
			{
				try {
					Main.getScene().setRoot((Parent) Main.getPanel("LoginPane"));




				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}
		});




	}


}

