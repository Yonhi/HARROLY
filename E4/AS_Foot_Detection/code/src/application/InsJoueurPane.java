package application;



import java.util.ArrayList;

import DAO.DAOBulletin;
import DAO.DAOCategories;
import DAO.DAOJoueur;
import DAO.DAORecruteur;
import DAO.DAOUtilisateur;
import classe.Bulletin;
import classe.Categorie;
import classe.Joueurs;
import classe.Recruteur;
import classe.Utilisateur;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;




public class InsJoueurPane extends GridPane {
	public InsJoueurPane() {


		this.setAlignment(Pos.TOP_CENTER);// le this sert a dire que c'est le grid de cette page
		this.setHgap(10);
		this.setVgap(10);
		this.setPadding(new Insets(25, 25, 25, 25));

		/*
		 * forme de l'interface
		 */
		String recruteur = "recruteur";
		
		Button Retour = new Button("Retour");
		this.add (Retour, 0, 0 );
		
		
		Text scenetitle = new Text("AS FOOT DETECTION  ");
		scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 70));
		this.add(scenetitle,10, 0, 2, 1);
		
		Text textCompte = new Text("Creer Votre Compte  ");
		textCompte.setFont(Font.font("Calibri", FontWeight.MEDIUM, 30));
		this.add(textCompte,10, 3, 2, 1);

		
		//formulaire
		Label labelLogin = new Label("Login");
		this.add(labelLogin, 10, 5);
		
		TextField login= new TextField();
		this.add(login, 11, 5,2,1);
		login.setMaxSize(200, 200);
		login.setLayoutX(100);
		
		Label labelEmail = new Label("Email");
		this.add(labelEmail, 10, 6);
		
		TextField email= new TextField();
		this.add(email, 11, 6,2,1);
		email.setMaxSize(200, 200);
		email.setLayoutX(100);
		
		Label labelMdp = new Label("Mot de passe");
		this.add(labelMdp, 10, 7);
		
		TextField mdp= new TextField();
		this.add(mdp, 11, 7,2,1);
		mdp.setMaxSize(200, 200);
		mdp.setLayoutX(100);
		
		Label labelPrenom = new Label("Prenom");
		this.add(labelPrenom, 10, 8);
		
		TextField prenom= new TextField();
		this.add(prenom, 11, 8,2,1);
		prenom.setMaxSize(200, 200);
		prenom.setLayoutX(100);
		
		
		Label labelNom = new Label("Nom");
		this.add(labelNom, 10, 9);
		
		TextField nom= new TextField();
		this.add(nom, 11, 9,2,1);
		nom.setMaxSize(200, 200);
		nom.setLayoutX(100);
		
		
		Label labelPoste = new Label("Poste");
		this.add(labelPoste, 10, 10);
		
		ComboBox listPoste = new ComboBox(); 
		listPoste.getItems().setAll("Attaquant","Milieu","Defenseur","Gardien");
		this.add(listPoste, 11, 10,2,1);
		
		
		Label labelClub = new Label("Club");
		this.add(labelClub, 10, 11);
		
		TextField club= new TextField();
		this.add(club, 11, 11,2,1);
		club.setMaxSize(200, 200);
		club.setLayoutX(100);
		
		Label labelCategorie = new Label("categorie");
		this.add(labelCategorie, 10, 12);
		ArrayList <String> allCat = null;
		DAOCategories dao = new DAOCategories();
		allCat = dao.getAllCategories();
		ComboBox listCat = new ComboBox();
		listCat.getItems().addAll(allCat);
		this.add(listCat, 11, 12,2,1);
		
		Label labelPoid = new Label("Poids");
		this.add(labelPoid, 10, 13);
		
		TextField poids= new TextField();
		this.add(poids, 11, 13,2,1);
		poids.setMaxSize(50, 50);
		poids.setLayoutX(50);
		
		Label labelButs = new Label("Nombre de Buts");
		this.add(labelButs, 10, 14);
		
		TextField but = new TextField();
		this.add(but, 11, 14,2,1);
		but.setMaxSize(50, 50);
		but.setLayoutX(50);
		
		Label labelPasse = new Label("Nombre de passe decisives");
		this.add(labelPasse, 10, 15);
		
		TextField passe = new TextField();
		this.add(passe, 11, 15,2,1);
		passe.setMaxSize(50, 50);
		passe.setLayoutX(50);
		
		Text textBulletin = new Text("Information sur votre bulletin de note ");
		textBulletin.setFont(Font.font("Calibri", FontWeight.MEDIUM, 20));
		this.add(textBulletin,12, 3, 2, 1);
		
		Label labelFrancais = new Label("francais");
		this.add(labelFrancais, 12, 5);
		
		TextField francais = new TextField();
		this.add(francais, 13, 5,2,1);
		francais.setMaxSize(50, 50);
		francais.setLayoutX(50);
		
		Label labelMaths = new Label("Maths");
		this.add(labelMaths, 12, 6);
		
		TextField  maths = new TextField();
		this.add(maths, 13, 6,2,1);
		maths.setMaxSize(50, 50);
		maths.setLayoutX(50);
		
		Label labelHistGeo = new Label("Histoire - geographie");
		this.add(labelHistGeo, 12, 7);
		
		TextField  histGeo = new TextField();
		this.add(histGeo, 13, 7,2,1);
		histGeo.setMaxSize(50, 50);
		histGeo.setLayoutX(50);
		
		Label labelAnglais = new Label("anglais");
		this.add(labelAnglais, 12, 8);
		
		TextField  anglais = new TextField();
		this.add(anglais, 13, 8,2,1);
		anglais.setMaxSize(50, 50);
		anglais.setLayoutX(50);
		
		Label labelTrimestre = new Label("trimestre");
		this.add(labelTrimestre, 12, 9);
		
		TextField  trimestre = new TextField();
		this.add(trimestre, 13, 9,2,1);
		trimestre.setMaxSize(50, 50);
		trimestre.setLayoutX(50);
		
		
		
		
			
			
			
			
			

	
		
		

		//bouton valider et s'incrire
		Button btnValide = new Button("Valider");
		this.add (btnValide, 11, 17 );
		btnValide.setMaxSize(150, 150);
		btnValide.setAlignment(Pos.CENTER);





		//gere les evenement 
		btnValide.setOnAction(new EventHandler<ActionEvent>()
		{
			@Override public void handle(ActionEvent e)
			{
				try {
					
					//j'initialise les differentes Dao pour avoir acces aux methodes
					
					DAOBulletin daoB = new DAOBulletin();
					DAOJoueur daoJ = new DAOJoueur();
					DAOUtilisateur daoU = new DAOUtilisateur();
					DAOCategories daoC = new DAOCategories();
					
					//j'initialise une categorie vide
					Categorie cat = null;
					
					//je stock la categorie que je recupere depuis la combox dans une variable de type Categorie
					cat = daoC.getCategorieByLibelle(listCat.getValue().toString());
					
					
					Utilisateur utl =  new Utilisateur (login.getText(),email.getText(),mdp.getText(),"joueur");
					daoU.insertUtilisateur(utl);
					
					
					
					
					Joueurs j = new Joueurs (login.getText(),nom.getText(),prenom.getText(),listPoste.getValue().toString(),
							club.getText(),poids.getText(),cat,but.getText(),passe.getText()); 
					Bulletin bul = new Bulletin(4,francais.getText(),maths.getText(),histGeo.getText(),anglais.getText()
							, trimestre.getText(),login.getText());
					
					//si execution = 1 changement de panel
					int execution = 0;
					execution = daoJ.insertJoueur(j, cat, utl);
					daoB.insertBulletin(bul, j);
					
					//condition qui permet d'accerder a la page Joueur
					if(execution==1) {
						Main.getScene().setRoot((Parent) Main.getPanel("JoueurPane"));
					}
					


				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}





			}
		});
		
		
		Retour.setOnAction(new EventHandler<ActionEvent>()
		{
			@Override public void handle(ActionEvent e)
			{
				try {
					
					Main.getScene().setRoot(Main.getPanel("InscriptionPane"));


				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}





			}
		});
		
		
		
		
	}


}

