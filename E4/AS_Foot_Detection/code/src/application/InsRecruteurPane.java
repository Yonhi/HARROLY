package application;



import java.util.ArrayList;

import DAO.DAOCategories;
import DAO.DAORecruteur;
import DAO.DAOUtilisateur;
import classe.Categorie;
import classe.Recruteur;
import classe.Utilisateur;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;




public class InsRecruteurPane extends GridPane {
	public InsRecruteurPane() {


		this.setAlignment(Pos.TOP_CENTER);// le this sert a dire que c'est le grid de cette page
		this.setHgap(10);
		this.setVgap(10);
		this.setPadding(new Insets(25, 25, 25, 25));

		/*
		 * forme de l'interface
		 */
		String recruteur = "recruteur";
		
		Button Retour = new Button("Retour");
		this.add (Retour, 4, 0 );
		
		Text scenetitle = new Text("AS FOOT DETECTION  ");
		scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 70));
		this.add(scenetitle,10, 0, 2, 1);
		
		Text textCompte = new Text("Creer Votre Compte  ");
		textCompte.setFont(Font.font("Calibri", FontWeight.MEDIUM, 30));
		this.add(textCompte,10, 3, 2, 1);

		Label labelLogin = new Label("Login");
		this.add(labelLogin, 10, 5);
		
		TextField login= new TextField();
		this.add(login, 11, 5,2,1);
		login.setMaxSize(200, 200);
		login.setLayoutX(100);
		
		Label labelEmail = new Label("Email");
		this.add(labelEmail, 10, 6);
		
		TextField email= new TextField();
		this.add(email, 11, 6,2,1);
		email.setMaxSize(200, 200);
		email.setLayoutX(100);
		
		Label labelMdp = new Label("Mot de passe");
		this.add(labelMdp, 10, 7);
		
		TextField mdp= new TextField();
		this.add(mdp, 11, 7,2,1);
		mdp.setMaxSize(200, 200);
		mdp.setLayoutX(100);
		
		Label labelClub = new Label("Club");
		this.add(labelClub, 10, 8);
		
		TextField club= new TextField();
		this.add(club, 11, 8,2,1);
		club.setMaxSize(200, 200);
		club.setLayoutX(100);
		
		Label labelCategorie = new Label("categorie");
		this.add(labelCategorie, 10, 9);
		

	     
		ArrayList <String> allCat = null;
		
		
			
			
			DAOCategories dao = new DAOCategories();
			allCat = dao.getAllCategories();
			
			ComboBox listCat = new ComboBox();
			listCat.getItems().addAll(allCat);
			this.add(listCat, 11, 9,2,1);
			
			
			
			
			

	
		
		

		//bouton valider 
		Button btnValide = new Button("Valider");
		this.add (btnValide, 11, 10 );
		btnValide.setMaxSize(150, 150);
		btnValide.setAlignment(Pos.CENTER);





		//gere les evenement 
		btnValide.setOnAction(new EventHandler<ActionEvent>()
		{
			@Override public void handle(ActionEvent e)
			{
				try {
					
					//j'initialise les differentes Dao pour avoir acces aux methodes
					
					DAOCategories daoC = new DAOCategories();
					DAORecruteur daoR = new DAORecruteur();
					DAOUtilisateur daoU = new DAOUtilisateur();
					
					//j'initialise une categorie vide
					Categorie cat = null;
					
					//je stock la categorie que je recupere depuis la combox dans une variable de type Categorie
					cat = daoC.getCategorieByLibelle(listCat.getValue().toString());
					
					
					Utilisateur utl =  new Utilisateur (login.getText(),email.getText(),mdp.getText(),"recruteur");
					daoU.insertUtilisateur(utl);
					
					
					Recruteur rct = new Recruteur(utl.getLogin(),club.getText(),cat);
					
					//si rctAjt = 1 changement de panel
					int rctAjt = daoR.insertRecruteur(rct,cat); 
					if(rctAjt == 1) {
						Main.getScene().setRoot((Parent) Main.getPanel("RecruteurPane"));
						
					}
					


				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}





			}
		});
		
		Retour.setOnAction(new EventHandler<ActionEvent>()
		{
			@Override public void handle(ActionEvent e)
			{
				try {
					
					Main.getScene().setRoot(Main.getPanel("InscriptionPane"));


				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}





			}
		});
		
		
		
		
	}


}

