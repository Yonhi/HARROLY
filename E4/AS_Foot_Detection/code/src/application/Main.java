package application;

import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Hashtable;

import application.JoueurPane;
import application.LoginPane;
import DAO.DAOJoueur;
import DAO.DAOUtilisateur;
import classe.Joueurs;
import classe.Utilisateur;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.stage.Stage;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;





import java.util.Hashtable;

public class Main extends Application {
    private static Scene scene;

    //creation de liste de panel
    private static Hashtable<String, Pane> vues = new Hashtable<>();
    
    //initialisation des panels
    private static void initVue() {
        vues.put("JoueurPane", new JoueurPane());
        vues.put("LoginPane", new LoginPane());
        vues.put("InscriptionPane", new InscriptionPane());
        vues.put("InsJoueurPane", new InsJoueurPane());
        vues.put("InsRecruteurPane", new InsRecruteurPane());
        vues.put("RecruteurPane", new RecruteurPane());
        vues.put("RecruteurProfilPane", new RecruteurProfilPane());
        vues.put("JoueurProfilPane", new JoueurProfilPane());
       
    }


    public static Pane getPanel(String s) {
        return vues.get(s);
    }

    public static Scene getScene() {
        return scene;
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        initVue();
        scene = new Scene(getPanel("LoginPane"), 700, 450);
        primaryStage.setTitle("LoginPane");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}


