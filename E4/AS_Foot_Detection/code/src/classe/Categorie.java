package classe;

public class Categorie {
	
	private String id;
	private  String libelle;
		
	/**
	 * constructeur avec en paremetre plusieurs parammetre
	 * @param id une chzaine de carractere
	 * @param libelle une chaine de carractere

	 */
		public Categorie(String id,String libelle) {
			this.id = id;
			this.libelle = libelle;
		}
		
		/**
		 * constructeur prenant aucun parametre
		 */
		public Categorie() {
			
		}


		/**methode qui recup�re l'id du joueur prenant aucun parametre
		 * @return l'id du joueur
		 */
		public String getId() {
			return id;
		}
		/**methode qui modifie l'id du joueur
		 * @param id une chaine de carractere
		 * 
		 */
		public void setId(String id) {
			this.id = id;
		}

		/**methode qui recup�re le libelle de la categorie prenant aucun parametre
		 * @return le libelle 
		 */
		public String getLibelle() {
			return libelle;
		}
		/**methode qui modifie libelle de la categorie 
		 * @param le libelle une chaine de carractere
		 * 
		 */
		public void setLibelle(String libelle) {
			this.libelle = libelle;
		}


		
		/**methode qui affiche touts les informations de la categorie prenant aucun parametres
		 * @return tous les attributs de la categorie dans une chaine de carractere
		 */
		@Override 
		public String toString() {
			return libelle ;
		}
		

	}
