package classe;

public class Bulletin {
	int id ;
	private String francais ;
	private String maths ;
	private String histGeo ;
	private String anglais;
	private String trimestre;
	private String idJoueur;
	/**
	 * constructeur avec en paremetre plusieurs parammetre
	 * @param francais une chzaine de carractere
	 * @param maths une chaine de carractere
	 * @param histGeo une chaine de carractere
	 * @param anglais une chaine de carractere
	 * @param trimestre une chaine de carractere
	 * 
	 * 
	 */

	public Bulletin (int id,String francais, String maths, String histGeo,String anglais,String trimestre,String idJoueur) {

		this.id = id ;
		this.francais = francais ;
		this.maths = maths;
		this.histGeo = histGeo;
		this.anglais = anglais;
		this.trimestre = trimestre;
		this.idJoueur = idJoueur;

	}



	/**methode qui recup�re numero de l'id du bulletin prenant aucun parametre
	 * @return le numero de licence
	 */
	public int getId() {
		return id;
	}

	
	/**methode qui modifie l'id du bellutin
	 * @param l'id en entier
	 * 
	 */
	public void setId(int id) {
		this.id = id;
	}


	/**methode qui recup�re numero de la note de francais du bulletin prenant aucun parametre
	 * @return le note de francais 
	 * 
	 */
	public String getFrancais() {
		return francais;
	}


	/**methode qui modifie la note de francais du bellutin
	 * @param francais une chaine de carractere
	 * 
	 */

	public void setFrancais(String francais) {
		this.francais = francais;
	}


	/**methode qui recup�re numero de la note de maths du bulletin prenant aucun parametre
	 * @return le note de maths
	 * 
	 */
	public String getMaths() {
		return maths;
	}


	/**methode qui modifie la note de maths du bellutin
	 * @param maths une chaine de carractere
	 * 
	 */

	public void setMaths(String maths) {
		this.maths = maths;
	}


	/**methode qui recup�re numero de la note de Histoire Geo du bulletin prenant aucun parametre
	 * @return le note d'histoire geo
	 * 
	 */
	public String getHistGeo() {
		return histGeo;
	}


	/**methode qui modifie la note d'histoire geo du bellutin
	 * @param histGeo une chaine de carractere
	 * 
	 */
	public void setHistGeo(String histGeo) {
		this.histGeo = histGeo;
	}


	/**methode qui recup�re numero de la note d'anglais du bulletin prenant aucun parametre
	 * @return le note d'anglais
	 * 
	 */
	public String getAnglais() {
		return anglais;
	}

	/**methode qui modifie la note d'anglais du bellutin
	 * @param anglais une chaine de carractere
	 * 
	 */
	public void setAnglais(String anglais) {
		this.anglais = anglais;
	}

	/**methode qui recup�re numero de le trimestre du bulletin prenant aucun parametre
	 * @return le trimestre
	 * 
	 */
	public String getTrimestre() {
		return trimestre;
	}




	/**methode qui modifie  le trimestre du bellutin
	 * @param trimestre une chaine de carractere
	 * 
	 */
	public void setTrimestre(String trimestre) {
		this.trimestre = trimestre;
	}


	/**methode qui recup�re l'id du joueur prenant aucun parametre
	 * @return l'id
	 */
	public String getIdJoueur() {
		return idJoueur;
	}


	/**methode qui modifie l'id du joueur
	 * @param idJoueur une chaine de carractere
	 * 
	 */
	public void setIdJoueur(String idJoueur) {
		this.idJoueur = idJoueur;
	}


	/**methode qui affiche touts les informations du bulletin prenant aucun parametres
	 * @return tous les attributs du buelletin dans une chaine de carractere
	 */
	public String toString () { 
		return "francais: " + this.francais; 
		}
}