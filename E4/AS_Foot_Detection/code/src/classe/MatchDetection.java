package classe;
import java.util.ArrayList;
import java.util.Date;  
public class MatchDetection {
	private String id ;
	private Categorie categorie ;
	private Date dateMatch ; 
	private ArrayList<Joueurs>listJ;
	
	public MatchDetection (String id,Categorie categorie, Date dateMatch,ArrayList<Joueurs>listJ ) {

		this.id = id ;
		this.categorie = categorie;
		this.dateMatch = dateMatch;
		this.listJ = listJ;
	}

	public ArrayList<Joueurs> getListJ() {
		return listJ;
	}

	public void setListJ(ArrayList<Joueurs> listJ) {
		this.listJ = listJ;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Categorie getCategorie() {
		return categorie;
	}

	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}

	public Date getDateMatch() {
		return dateMatch;
	}

	public void setDateMatch(Date dateMatch) {
		this.dateMatch = dateMatch;
	}
	public String toString () { 
		return  " club : "+ this.dateMatch+ categorie.toString(); 
		}

}
