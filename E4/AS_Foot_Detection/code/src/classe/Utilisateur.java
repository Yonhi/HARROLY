package classe;

import java.util.Date;
import classe.Categorie;
public class Utilisateur {

	protected String login;
	protected String email;
	protected String mdp;
	protected String type;



	/**
	 * constructeur avec en paremetre plusieurs parammetre
	 * @param login une chzaine de carractere
	 * @param email une chaine de carractere
	 * @param mdp une chaine de carractere
	 * @param type une chaine de carractere
	 */

	public Utilisateur(String login,String email,String mdp,String type) {

		this.login = login;
		this.email = email;
		this.mdp = mdp;
		this.type = type;


	}
	/**
	 * constructeur prenant aucun parametre
	 */
	public Utilisateur() {



	}

	
	/**methode qui affiche touts les informations de l'utilisateur prenant aucun parametres
	 * @return tous les attributs du l'utilisateur dans une chaine de carractere
	 */
	public String toString() {
		return login+"\n"+"prenom:"+email+"\n"+mdp+"\n";
	}

	/**methode qui recup�re le login de l'utlisateur prenant aucun parametre
	 * @return l'id du joueur
	 */
	public String getLogin() {
		return login;
	}
	/**methode qui modifie le login de l'utilisateur
	 * @param login une chaine de carractere
	 * 
	 */
	public void setLogin(String login) {
		this.login = login;
	}


	
	 /**methode qui recup�re l'email de l'utlisateur prenant aucun parametre
	 * @return l'email de l'utilisateur
	 */
	public String getEmail() {
		return email;
	}
	/**methode qui modifie l'email de l'utilisateur
	 * @param mot de passe une chaine de carractere
	 * 
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	 /**methode qui recup�re le mot de passe de l'utlisateur prenant aucun parametre
	 * @return le mot de passe de l'utilisateur
	 */
	public String getMdp() {
		return mdp;
	}
	/**methode qui recup�re le mot de passe de l'utlisateur prenant aucun parametre
	 * @return le mot de passe de l'utilisateur
	 */
	public void setMdp(String mdp) {
		this.mdp = mdp;
	}

	/**methode qui recup�re le type de l'utlisateur prenant aucun parametre
	 * @return le type de l'utilisateur
	 */
	public String getType() {
		return type;
	}
	/**methode qui modifie le type de l'utilisateur
	 * @param type une chaine de carractere
	 * 
	 */
	public void setType(String type) {
		this.type = type;
	}

}


