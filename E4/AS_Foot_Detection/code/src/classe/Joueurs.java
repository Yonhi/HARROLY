package classe;

import classe.Utilisateur;
import jdk.internal.dynalink.ChainedCallSite;

import java.util.ArrayList;

import classe.Categorie;
public class Joueurs extends Utilisateur {
	private String id ;  
	private String nom ;
	private String prenom ;
	private String poste ;
	private String club ;
	private String poids;
	private Categorie categorie;
	private String nbButs ;
	private String nbPasseD ;
	

	
	
	/**
	 * constructeur avec en paremetre les attributs de la mere et ces popres attributs
	 * @param login une chzaine de carractere
	 * @param email une chaine de carractere
	 * @param mdp une chaine de carractere
	 * @param type une chaine de carractere
	 * @param id une chaine de carractere
	 * @param nom une chzaine de carractere
	 * @param prenom une chaine de carractere
	 * @param nom une chaine de carractere
	 * @param poste une chaine de carractere
	 * @param club une chaine de carractere
	 * @param categorie plusieurs chaine de carractere de la classe Categorie
	 * @param poids une chzaine de carractere
	 * @param buts une chaine de carractere
	 * @param passes une chaine de carractere 
	 */
	public Joueurs (String login,String email,String mdp ,String type,String id,String nom,String prenom,String poste,String club, String poids,Categorie categorie, String nButs,String nbPasseD) {
		super (login,email,mdp,type);
		this.id = id ;
		this.nom = nom ;
		this.prenom = prenom;
		this.club = club;
		this.poids = poids;
		this.categorie = categorie;
		this.nbButs = nbButs;
		this.nbPasseD = nbPasseD;
		
	}
	
	public Joueurs( String id, String nom, String prenom,
			String poste, String club,String poids, Categorie categorie, String nbButs, String nbPasseD) {
		
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.poste = poste;
		this.club = club;
		this.poids = poids;
		this.categorie = categorie;
		this.nbButs = nbButs;
		this.nbPasseD = nbPasseD;
	}
	/**
	 * constructeur prenant aucun parametre
	 */
	public Joueurs() {
		
		
	}
	
	
	/**methode qui recup�re l'id du joueur prenant aucun parametre
	 * @return l'id du joueur
	 */
	public String getId() {
		return id;
	}
	
	
	/**methode qui modifie l'id du joueur
	 * @param id une chaine de carractere
	 * 
	 */
	public void setId(String id) {
		this.id = id;
	}

	
	
	/**methode qui recup�re numero de le nom du joueur prenant aucun parametre
	 * @return le nom du joueur
	 */
	public String getNom() {
		return nom;
	}

	/**methode qui modifie le nom du joueur
	 * @param le nom une chaine de carractere
	 * 
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	
	
	/**methode qui recup�re le prenom du joueur prenant aucun parametre
	 * @return le prenom du joueur
	 */
	public String getPrenom() {
		return prenom;
	}
	/**methode qui modifie le prenom du joueur
	 * @param le prennom une chaine de carractere
	 * 
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	
	
	/**methode qui recup�re le poste du joueur prenant aucun parametre
	 * @return le poste du joueur
	 */
	public String getPoste() {
		return poste;
	}
	/**methode qui modifie le poste du joueur
	 * @param le poste une chaine de carractere
	 * 
	 */
	public void setPoste(String poste) {
		this.poste = poste;
	}

	
	
	/**methode qui recup�re le club du joueur prenant aucun parametre
	 * @return le club du joueur
	 */
	public String getClub() {
		return club;
	}
	/**methode qui modifie le club du joueur
	 * @param le club une chaine de carractere
	 * 
	 */
	public void setClub(String club) {
		this.club = club;
	}

	
	
	/**methode qui recup�re la categorie du joueur prenant aucun parametre
	 * @return la categorie du joueur
	 */
	public Categorie getCategorie() {
		return categorie;
	}
	/**methode qui modifie la categorie du joueur
	 * @param la categorie de type Categorie
	 * 
	 */
	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}

	
	
	/**methode qui recup�re le poids du joueur prenant aucun parametre
	 * @return le poids du joueur
	 */
	public String getPoids() {
		return poids;
	}
	/**methode qui modifie le poids du joueur
	 * @param le poids une chaine de carractere
	 * 
	 */
	public void setPoids(String poids) {
		this.poids = poids;
	}


	
	
	/**methode qui recup�re le nombre de buts du joueur prenant aucun parametre
	 * @return le nombre de buts du joueur
	 */
	public String getNbButs() {
		return nbButs;
	}
	/**methode qui modifie le nombre de buts du joueur
	 * @param le nombre de buts une chaine de carractere
	 * 
	 */
	public void setNbButs(String nbButs) {
		this.nbButs = nbButs;
	}

	
	/**methode qui recup�re le nombre de passes decisives du joueur prenant aucun parametre
	 * @return le nombre de passes decisives du joueur
	 */
	public String getNbPasseD() {
		return nbPasseD;
	}
	/**methode qui modifie le nombre de passes du joueur
	 * @param le nombres de passes une chaine de carractere
	 * 
	 */
	public void setNbPasseD(String nbPasseD) {
		this.nbPasseD = nbPasseD;
	}

	/**methode qui affiche touts les informations du joueurs prenant aucun parametres
	 * @return tous les attributs du joueur dans une chaine de carractere
	 */
	@Override   
	public String toString() {
		return "id :" + id  +"\n"+  " nom : " + nom + "\n"+" prenom : " + prenom +"\n"+ " poste : " + poste +"\n"+ " club : " + club
				+"\n"+ " poids : " + poids +"\n"+ " categorie :" + categorie +"\n"+ " nbButs :" + nbButs + "\n"+" nbPasseD :" + nbPasseD
				+ "\n";
	}

	
	
	

}
