 package classe;

public class Recruteur {
	private String id ;
	private String club;
	private Categorie categorie ;
	
	
	/**
	 * constructeur avec en paremetre plusieurs parammetre
	 * @param id une chzaine de carractere
	 * @param club une chaine de carractere
	 * @param categorie une chaine de carractere
	 */
	public Recruteur (String id,String club,Categorie categorie) {

		this.id = id ;
		this.club = club ;
		this.categorie = categorie;
		
	}

	
	
	/**methode qui recup�re l'id du joueur prenant aucun parametre
	 * @return l'id du joueur
	 */
	public String getId() {
		return id;
	}
	/**methode qui modifie l'id du joueur
	 * @param id une chaine de carractere
	 * 
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	
	/**methode qui recup�re le club du joueur prenant aucun parametre
	 * @return le club du joueur
	 */
	public String getClub() {
		return club;
	}
	/**methode qui modifie le club du joueur
	 * @param le club une chaine de carractere
	 * 
	 */
	public void setClub(String club) {
		this.club = club;
	}
	/**methode qui modifie le club du joueur
	 * @param le club une chaine de carractere
	 * 
	 */
	public Categorie getCategorie() {
		return categorie;
	}
	/**methode qui modifie la categorie du joueur
	 * @param la categorie de type Categorie
	 * 
	 */
	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}
	
	/**methode qui affiche touts les informations du joueurs prenant aucun parametres
	 * @return tous les attributs du joueur dans une chaine de carractere
	 */
	public String toString () { 
		return super.toString()+ " club : "+ this.club + categorie.toString(); 
		}

}
