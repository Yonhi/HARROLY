-- Adminer 4.6.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `bulletin_de_note`;
CREATE TABLE `bulletin_de_note` (
  `id` int(11) NOT NULL,
  `francais` double DEFAULT NULL,
  `maths` double DEFAULT NULL,
  `histgeo` double DEFAULT NULL,
  `anglais` double DEFAULT NULL,
  `trimestre` int(11) DEFAULT NULL,
  `id_joueur` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_joueur` (`id_joueur`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` varchar(30) NOT NULL,
  `libelle` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `categories_recruteurs`;
CREATE TABLE `categories_recruteurs` (
  `id_recruteur` varchar(30) NOT NULL,
  `id_categorie` varchar(30) NOT NULL,
  PRIMARY KEY (`id_recruteur`,`id_categorie`),
  KEY `id_categorie` (`id_categorie`),
  CONSTRAINT `categories_recruteurs_ibfk_1` FOREIGN KEY (`id_recruteur`) REFERENCES `recruteurs` (`id`),
  CONSTRAINT `categories_recruteurs_ibfk_2` FOREIGN KEY (`id_categorie`) REFERENCES `categories` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `centre_detection`;
CREATE TABLE `centre_detection` (
  `id` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `commentaire_recruteurs`;
CREATE TABLE `commentaire_recruteurs` (
  `id_joueurs` varchar(30) NOT NULL,
  `id_recruteurs` varchar(30) NOT NULL,
  `commentaires` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_joueurs`,`id_recruteurs`),
  KEY `id_recruteurs` (`id_recruteurs`),
  CONSTRAINT `commentaire_recruteurs_ibfk_1` FOREIGN KEY (`id_joueurs`) REFERENCES `joueurs` (`id`),
  CONSTRAINT `commentaire_recruteurs_ibfk_2` FOREIGN KEY (`id_recruteurs`) REFERENCES `recruteurs` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `joueurs`;
CREATE TABLE `joueurs` (
  `id` varchar(30) NOT NULL,
  `nom` varchar(30) DEFAULT NULL,
  `prenom` varchar(30) DEFAULT NULL,
  `poste` varchar(30) DEFAULT NULL,
  `club` varchar(30) DEFAULT NULL,
  `poids` double DEFAULT NULL,
  `categorie` varchar(30) DEFAULT NULL,
  `nb_Buts` int(11) DEFAULT NULL,
  `nb_Passe_D` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `categorie` (`categorie`),
  CONSTRAINT `joueurs_ibfk_3` FOREIGN KEY (`categorie`) REFERENCES `categories` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `match_detection`;
CREATE TABLE `match_detection` (
  `id` varchar(30) NOT NULL,
  `categorie` varchar(30) DEFAULT NULL,
  `date_match` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `categorie` (`categorie`),
  CONSTRAINT `match_detection_ibfk_1` FOREIGN KEY (`categorie`) REFERENCES `categories` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `recruteurs`;
CREATE TABLE `recruteurs` (
  `id` varchar(30) NOT NULL,
  `club` varchar(30) DEFAULT NULL,
  `categorie` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `categorie` (`categorie`),
  CONSTRAINT `recruteurs_ibfk_3` FOREIGN KEY (`id`) REFERENCES `utilisateur` (`login`),
  CONSTRAINT `recruteurs_ibfk_4` FOREIGN KEY (`categorie`) REFERENCES `categories` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE `utilisateur` (
  `login` varchar(30) NOT NULL,
  `email` varchar(30) DEFAULT NULL,
  `mdp` varchar(30) DEFAULT NULL,
  `categorie_utilisateur` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `utilisateur` (`login`, `email`, `mdp`, `categorie_utilisateur`) VALUES
('ABDL',	'abidal@gmail.com',	'mdp',	'recruteur'),
('Adrien',	'A.Rabiot@gmail.com',	'mdp',	'joueur'),
('AlexL',	'alex@gmail.com',	'mdp',	'joueur'),
('Arjen',	'Rob@gmail.com',	'mdp',	'joueur'),
('BlancL',	'BLanc@gmail.com',	'mdp',	'recruteur'),
('cortin',	'toms@gmail.com',	'mdp',	'joueur'),
('Daren',	'Neres@gmail.com',	'mdp',	'joueur'),
('Emey',	'unai.e@gmail.com',	'mdp',	'recruteur'),
('Gels16',	'Martins@gmail.com',	'mdp',	'joueur'),
('hhh',	'hh',	'jjj',	'recruteur'),
('kylian',	'kbmp',	'mdpm@gmail.com',	'joueur'),
('login',	'id',	'id',	'type'),
('Logintst',	'emailtst',	'mdp',	'recruteur'),
('LOL',	'LOL@gmail.com',	'mdp',	'recruteur'),
('Lucas',	'lucas@gmail.com',	'mdp',	'joueur'),
('lyly',	'lala@gmail.com',	'mdp',	'joueur'),
('mat75',	'mat@gmail.com',	'mdp',	'joueur'),
('MDR',	'MDR@gmail.com',	'mdp',	'recruteur'),
('med',	'lala@gmail.com',	'mdp',	'joueur'),
('Moise',	'Kean@gmail.com',	'mdpk',	'joueur'),
('mong',	'lala@gmail.com',	'mdp',	'joueur'),
('Motta',	'motta@gmail.com',	'mdp',	'recruteur'),
('Nabil69',	'fekir@gmail.com',	'mdp',	'joueur'),
('Nene',	'Neres@gmail.com',	'mdp',	'joueur'),
('RecruteurLogin',	'recruteur@gmail.com',	'mdp',	'recruteur'),
('Ronni',	'Lopes@hotmail.fr',	'mdp',	'joueur'),
('RTest2',	'email',	'mdp',	'Recruteur'),
('Silva',	'Silva@gmail.cocm',	'test',	'recruteur'),
('SilvaS',	'Silva@gmail.com',	'mdp',	'recruteur'),
('TEST',	'TST@gmail.com',	'mdp',	'joueur'),
('TestUser',	'email',	'mdp',	'Recruteur'),
('Timo',	'Timo.W@gmail.com',	'mdp',	'joueur'),
('UTL',	'UTL@gmail.com',	'mdp',	'joueur'),
('yonhi',	'y.harroly@gmail.com',	'test',	'joueur');

-- 2019-05-18 10:43:12
