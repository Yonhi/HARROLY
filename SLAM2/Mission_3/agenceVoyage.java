package mission3;
import java.util.ArrayList;

public class AgenceVoyage {
	private String nom;
	private AdressePostale adresse;
	private ArrayList<Voyageurs>listV;
	private Voyageurs voy;
	
	//étape initialisation du constructeur
	AgenceVoyage (String nom,AdressePostale adresse ) {
		listV = new ArrayList<Voyageurs>();
		this.nom = nom;
		this.adresse = adresse ; 
	}
	
	public void setNom(String nom) {
		this.nom = nom; 
		
	}
	public String getNom() {
		return this.nom;
	}
	
	public void setAdresse(AdressePostale adresse) {
		this.adresse= adresse; 
		
	}
	public AdressePostale  getAdressePosale() {
		return this.adresse;
	}
	public void setListV(ArrayList ListV) {
		this.listV = ListV; 
		
	}
	public ArrayList getListV() {
		return this.listV;
	}
	
	public void addVoyageur(Voyageurs voy) {
		listV.add(voy);
	
}
	
	public void  delNomVoyageurs(String nom) {
		for(int i=0 ;i< listV.size() ;i++){
			Voyageurs p= listV.get(i);
			if(nom.equals(p.getNom())) {//condition qui prend le nom de chaque voyageurs
				listV.remove(p);
				}
			}
		
	}
	
	public void setVoyageur(Voyageurs voy) {
		this.voy = voy ;
		
	}
	public Voyageurs getVoyageurs() {
		return this.voy;
	}
	public void  getNomVoyageurs(String nom) {
		for(int i=0 ;i< listV.size() ;i++){
			Voyageurs p= listV.get(i);
			if(nom.equals(p.getNom())) {//condition qui prend le nom de chaque voyageurs
				p.afficher();
				}
			}
		
	}
	
	void Afficher() {
		System.out.println("Nom de l'agence de voyage : "+nom);
		adresse.Afficher();
		for(int i=0 ;i< listV.size() ;i++){
			Voyageurs p= listV.get(i);// p est la varible pour chaque indice de la boucle
			p.afficher();
			System.out.println(" ");
			}
		
		
	}
	

}
