package TestJDBC;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class JDBCSimple {
	public static void main(String[] args) {
		try{
		Class.forName("com.mysql.cj.jdbc.Driver");
		System.out.println("driver ok");
		Connection conn=
		DriverManager.getConnection("jdbc:mysql://sl-us-south-1-portal.10.dblayer.com:25147/BTS?" +
                "user=admin&password=JUIUCVSOPSVNEAEZ");
		System.out.println("connection ok");
		//suite du code
		/*1ère requete
		 * execution d'une requete 
		 * insertion de donnes dans la table 
		 */
		/*Statement stmt = conn.createStatement();
		String req1 = "INSERT INTO professeur VALUES (32,'Abdelmoula','SLAM')";
		int res = stmt.executeUpdate(req1);
		System.out.println("nb de modifications réalisées : " + res);
		*/
		
		//affiche le contenue de la table avec SELECT
		Statement stmt = conn.createStatement();
		String req2= "SELECT* FROM professeur";
		ResultSet result = stmt.executeQuery(req2);
		// On affiche les donneés des différentes colonnes
		while(result.next()){
		System.out.println( "id: "+ result.getString(1));	
		System.out.println( "Nom : "+ result.getString(2));// la premiere colonne 
		System.out.println( "Spécialité : "+ result.getString(3));
		
		}
		result.close();
		stmt.close();
		
	
		}catch(Exception e){
		e.printStackTrace();
		}

		}
}