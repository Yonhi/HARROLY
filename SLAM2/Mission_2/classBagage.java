package mission3;

public class Bagage {
	int numB;
	String couleur;
	double poids;
	
	Bagage(int n, String c, double p){
		numB = n;
		couleur = c;
		poids = p;
	}
	
	Bagage(){
		numB = 0;
		couleur = " ";
		poids = 0;
	}
	public void setNumB(int num){
		this.numB= num;
		}
	int getNumB() {
		return this.numB;
	}
	public void setCouleur(String c){
		this.couleur= c;
		}
	String getCouleur() {
		return this.couleur;
	}

	public void setPoids(double p){
		this.poids= p;
		}
	double getPoids() {
		return this.poids;
	}
	void AfficherBag() {
		System.out.print("Bagage :" +numB +" - "+couleur+" - "+poids);
	}
}

