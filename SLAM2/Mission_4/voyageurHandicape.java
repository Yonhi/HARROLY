package mission4;

public class VoyageurHandicape extends Voyageurs {
	
	String typeHdcp;
	
	public VoyageurHandicape (String nom,int age, AdressePostale adresse,Bagage bag, String typeHdcp) {
		super (nom,age,adresse,bag);
		this.typeHdcp = typeHdcp;
	}
	public String toString () { 
		return super.toString()+ " handicape: "+ this.typeHdcp; 
		}
	
	public String getTypeHdcp() {
		return this.typeHdcp;
	}

	public void setTypeHdcp(String typeHdcp) {
		this.typeHdcp = typeHdcp;
	}
}
