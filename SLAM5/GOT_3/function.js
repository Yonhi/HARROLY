$(document).ready(function(){

    $('#btn-get-villes').click(function () {

        $('#table-villes').bootstrapTable({

            url: 'https://api.got.show/api/cities/',

            dataType: "json",

            columns: [{

                field: 'name',

                title: 'name of cities'

            }, {
            	field: 'type',
                title: 'type of cities'
            },]
        });
    });
});

$(document).ready(function(){

    $('#btn-get-maison').click(function () {

        $('#table-maison').bootstrapTable({

            url: 'https://api.got.show/api/houses/',

            dataType: "json",

            columns: [{

                field: 'name',

                title: 'name of houses'

            }, {
            	field: 'type',
                title: 'type of houses'
            },]
        });
    });
});


$(document).ready(function(){
    $('#rechVille').click(function () {

         $.ajax({
            
            type: "GET",

            url: 'https://api.got.show/api/cities/'+$('#byname').val(),
            success:function(resultat)
            {
               $('#resultat').html(resultat.data.name +"<br>" 
                + resultat.data._id + "<br> "
                + resultat.data.coordX +"<br> "
                + resultat.data.coordY +"<br> "
                + resultat.data.type +"<br> "
                + resultat.data.priority +"<br> "
                + resultat.data.link);
            },
        });
    });
});


$(document).ready(function(){
    $('#rechPerso').click(function () {

         $.ajax({
            
            type: "GET",

            url: 'https://api.got.show/api/characters/'+$('#LePerso').val(),
            success:function(resultat)
            {
               $('#resultat').html(resultat.data.name +"<br>" 
                + resultat.data._id + "<br> "
                + resultat.data.male +"<br> "
                + resultat.data.__v +"<br> "
                + resultat.data.pageRank +"<br> "
                + resultat.data.books +"<br> "
                + resultat.data.updatedAt+"<br>"
                + resultat.data.createdAt+"<br>"
                + resultat.data.titles);
            },
        });
    });
});