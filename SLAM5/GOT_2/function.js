import {Utilisateur} from "./Utilisateur.js";

$("form").submit(function (event) {
    let destination = $(this).attr('action');
    let login = $("#login").val();
    let password = $("#password").val();
    let resultat = $("#resultat");


    $.ajax({
        method: "POST",
        url: destination,
        data: {login: login, password: password},
        dataType: 'json',
        success: function (data) {
            if (data.resultat === "ok") {
                let utilisateur = new Utilisateur(login, data.email);
                window.location = "mesPersonnages.php";
            } else {
                $("#resultat").html('Identifiants incorrect');
            }
        }
    });

    event.preventDefault();
});

$("#login").attr('value', localStorage.getItem("Login"));
$("#info").html("Login: " + localStorage.getItem("Login") + "<br>" + "Email: " + localStorage.getItem("Email"));
