<?php
session_start();

if (isset($_POST['login']) && isset($_POST['password'])) {


    $host = "localhost";
    $user = "root";
    $pass = "";
    $table = "utilisateur";
    $pdo = null;

    try {
        $pdo = new \PDO('mysql:host=' . $host . ';dbname=' . $table . ';charset=utf8mb4', $user, $pass, [
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
            \PDO::ATTR_PERSISTENT => true,
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION
            // \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8mb4'
        ]);
    } catch (\PDOException $e) {
        die('Erreur lors de la connexion &agrave; la base de donn&eacute;es: ' . $e->getMessage());
    }

    $login = $_POST['login'];
    $password = $_POST['password'];
    $resultat = null;
    $email = null;


    $requete = $pdo->prepare('SELECT email FROM got_utilisateur WHERE login = ? AND motdepasse = ?');
    $requete->execute([$login, $password]);
    if ($requete->rowCount() > 0) {
        $fetch = $requete->fetch();
        $email = $fetch['email'];
        $resultat = "ok";
        $_SESSION['connecte'] = true;
    } else {
        $resultat = "error";
    }

    echo json_encode(["resultat" => $resultat, "email" => $email]);
}


?>