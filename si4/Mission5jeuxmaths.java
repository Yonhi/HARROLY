﻿QUESTION 1


import java.util.Scanner;

public class jeuxmaths {
	public static void main (String[] args){
		Scanner sc = new Scanner(System.in);

		int indice;
		int nb=0;
		String nbsaisie="";
		int resultat=0;

		for (indice=0; indice<10;indice++){
			System.out.println("saisir un nombre");
			nb= sc.nextInt();
			nbsaisie = nb + "+"+nbsaisie;
			resultat = resultat +nb;
			if (indice ==0){
				nbsaisie= nbsaisie+nb;
			}

		}

		System.out.println(nbsaisie + "="+resultat);

	}
}

/*
variable : indice,nb,resultat(entiers), nbsaisie(chaine de caractere)
début:
      nb=0
      resultat=0
      Pour indice de 0 a 5 avec pas 1
         faire 
              afficher "saisir un nombre"
              saisir nb
              nbaisie = nb+ "+"+nbsaisie
              resultat = resultat + nb
              Si indice = 0
                 alors
                  nbsaisie=nbsaisie+nb
              Fin si
      Fin pour
      afficher nbaisie + "="+resultat
      
     */


QUESTION 2

import java.util.Scanner;

public class jeuxmaths {
	public static void main (String[] args){
		Scanner sc = new Scanner(System.in);
		int indice;
		int a;
		String operation= "";
		int choix;
		int somme=0;
		
				
		
		for (indice=0 ;indice <5; indice ++){
			System.out.println("choisir un nombre");
			a = sc.nextInt();
			if (indice==0) {
				operation=operation+a;
			}
			System.out.println("Voulez vous additionner(1) ou sosutraire(2) ");
			choix = sc.nextInt();
			while (choix<1 || choix >2) {
				System.out.println(" ERREUR: additionner(1) ou sosutraire(2)");
				choix = sc.nextInt();
				}
			
			if (choix==1){
				operation=operation +("+") +a;
				somme = somme + a;
				}
			if(choix== 2){
				operation=operation + ("-") +a;
				somme=somme-a; 
			}
		 }
		System.out.print(operation+ "="+somme);
		}
    }

Algo question2 
/*
variables: indice,a,choix, sommes(entiers) operation (chaine de caractère)
Début:
     operation = " "
     sommes =0
     Pour indice de 0 à 5 avec pas de 1
         faire
           afficher "choisir nombre"
           saisir a 
           Si indice = 0
             alors
             operation = operation+a
           Fin si
           afficher"voulez vous additionner(1) ou soustraire(2)"
           saisir choix
           Tant que choix<1 et choix >2
             faire
             afficher "ERREUR: additionner(1) ou sosutraire(2)"
             saisir choix
           Fin tant que
           Tant que choix=1
             faire
             operation = operation+"+"+a
             somme = somme + a
           Fin tant que 
            Fin tant que
           Tant que choix=2
             faire
             operation = operation+"-"+a
             somme = somme - a
           Fin tant que 
           Afficher operation + "="+somme
           
*/           
           

QUESTION 3

package si4;

import java.util.Scanner;
import java.util.Random;

public class jeuxmaths {
	public static void main (String[] args){
		Scanner sc = new Scanner(System.in);
		int indice;
		Random r = new Random();
		int n = r.nextInt(100);
		int a;
		int nbessaie=0;
		System.out.println("Saisir un nombre entre 1 et 100");
		System.out.println("Vous avez 12 essaie!");
		
		for (indice=0 ;indice <12; indice ++) {
			a = sc.nextInt();
			if (indice==11) {
				System.out.println("Vous n'avez pas trouver le nombre exact");
			}
			if (indice>=0 && indice<11) {

				if (a<n) {
					System.out.println("le nombres est superieur");
				}
				else if (a>n) {
					System.out.println("le nombres est inferieur");
				}
				else if (a==n) {
					System.out.println("Bravo vous avez trouvé le nombre exat!");
					System.out.println("Vous avez fait indice" +" "+ indice+" "+ "essaies");
					System.exit(0);
				    
				}
			    
			}
		    
		}
	    
	}
}
