package joueurfoot;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.stage.Stage;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;




public class Main extends Application {

	public static void main(String[] args) {//methode qui appelle la fontion launch qui lance le reste du programme
		Application.launch(Main.class, args);
	}


	@Override
	public void start (Stage primaryStage) { //methode qui appelle la fonction launch() qui prend un objet de type stage (le theatre de l'application)
		
		try{
			JoueurDAO dao = new JoueurDAO ();

		

			/*
			 * creation du layout sous forme de lignes et colonnes
			 */
			GridPane grid = new GridPane();

			grid.setAlignment(Pos.TOP_CENTER);
			grid.setHgap(10);
			grid.setVgap(10);
			grid.setPadding(new Insets(25, 25, 25, 25));

			/*
			 * forme de l'interface
			 */
			Text scenetitle = new Text("Joueur Foot ");
			scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 100));
			grid.add(scenetitle, 0, 0, 2, 1);

			//champs texte
			//bouton valider
			TextField textF= new TextField();
			grid.add(textF, 1, 1);
			
			Button btn = new Button("Valider");
		
			grid.add (btn, 1, 3);
			
			Text affJoueur = new Text();
		    grid.add(affJoueur, 1, 4);
		
			

			//gere les evenement 
			btn.setOnAction(new EventHandler<ActionEvent>()
			{
				@Override public void handle(ActionEvent e)
				{
				try {
					dao.getJoueurById(textF.getText());
					JoueurFoot j = dao.getJoueurById(textF.getText());
					String url = "http://www.popstickers.fr/292-large_default/footballeur.jpg";
					Image image = new Image(url,100,100,false,true);
					ImageView iv = new ImageView(image);
					grid.add(iv, 2, 4);
					
				
					affJoueur.setText(j.toString());
				} catch (DaoException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}			
					


				}
			});
			



			/*
			 * on cree la scene qui contient le GridPane qui contiendra les objets
			 * on ajoute lka scene a l'objet stage qui coresspond au theatre
			 */
			Scene scene = new Scene(grid, 300, 275);
			primaryStage.setScene(scene);
			primaryStage.setTitle("Joueur Foot");

			primaryStage.show ();
		}catch(Exception e){
			e.printStackTrace();
		
		}

	}
}

