package joueurfoot;

import java.util.Date;  

/**
 * 
 * @author yonhi
 * @version 2.0
 */

public class JoueurFoot {
	private int numLicence;
	private String nom;
	private String prenom;
	private String poste;
	private int num;
	private String club;
	private Date dateDeNaissance;

	
	/**
	 * constructeur avec en paremetre plusieurs parammetre
	 * @param numLicence un entier
	 * @param nom une chaine de carractere
	 * @param prenom une chaine de carractere
	 * @param poste une chaine de carractere
	 * @param num un entier
	 * @param club une chaine de carractere
	 * @param dateDeNaissance une date
	 * 
	 */
	public JoueurFoot(int numLicence,String nom,String prenom,String poste,int num,String club,Date dateDeNaissance) {
        this.numLicence = numLicence;
        this.nom = nom;
        this.prenom = prenom;
        this.poste = poste;
        this.num = num;
        this.club = club;
        this.dateDeNaissance = dateDeNaissance;
        
    }
	
	 public JoueurFoot(){

	    }
	 
	/**methode qui recup�re numero de licence du joueur prenant aucun parametre
	 * @return le numero de licence
	 */
	public int getNumLicence() {
		return numLicence;
	}

	/**methode qui modifie le numero de licence 
	 * @param numLicence en entier
	 * 
	 */
	public void setNumLicence(int numLicence) {
		this.numLicence = numLicence;
	}

	/**methode qui recup�re nom joueur prenant aucun parametre
	 * @return le nom
	 */
	public String getNom() {
		return nom;
	}

	/**methode qui modifie le nom du joueur
	 * @param nom une chaine de carracter
	 * 
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**methode qui recup�re le prenom joueur prenant aucun parametre
	 * @return le prenom
	 */
	public String getPrenom() {
		return prenom;
	}

	/**methode qui modifie le prenom du joueur
	 * @param prenom une chaine de carracter
	 * 
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	/**methode qui recup�re le poste joueur prenant aucun parametre
	 * @return le poste
	 */
	public String getPoste() {
		return poste;
	}

	/**methode qui modifie le poste du joueur
	 * @param poste une chaine de carracter
	 * 
	 */
	public void setPoste(String poste) {
		this.poste = poste;
	}

	/**methode qui recup�re le numero du joueur prenant aucun parametre
	 * @return le numeroe
	 */
	public int getNum() {
		return num;
	}
	
	/**methode qui modifie le numero du joueur
	 * @param num un entier
	 * 
	 */
	public void setNum(int num) {
		this.num = num;
	}

	/**methode qui recup�re le club joueur prenant aucun parametre
	 * @return le club
	 */
	public String getClub() {
		return club;
	}

	/**methode qui modifie le club du joueur
	 * @param club une chaine de carracter
	 * 
	 */
	public void setClub(String club) {
		this.club = club;
	}

	/**methode qui recup�re la date de naissance du joueur prenant aucun parametre
	 * @return le poste
	 */
	public Date getDateDeNaissance() {
		return dateDeNaissance;
	}

	/**methode qui modifie la date de naissance du joueur
	 * @param dateDeNaissance une date
	 * 
	 */
	public void setDateDeNaissance(Date dateDeNaissance) {
		this.dateDeNaissance = dateDeNaissance;
	}
	
	/**methode qui recup�re le poste joueur prenant aucun parametre
	 * @return le poste
	 */
   public String toString() {
	   return "Numero le licence:  "+numLicence+"\n"+ "nom: "+nom+"\n"+"prenom:"+prenom+"\n"+"poste: "+poste+"\n"+"numero: "+num+"\n"+"club: "+club+"\n"+"Date de naissance: "+dateDeNaissance;
   }
	
	}



