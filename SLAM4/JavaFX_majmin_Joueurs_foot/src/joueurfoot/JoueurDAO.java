package joueurfoot;
import joueurfoot.DaoException;
import joueurfoot.JoueurFoot;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Date;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;




public class JoueurDAO {
	/**
	 * methode qui permet de se connecter a la base de donn�es qui prend rien en parametre
	 * @return conn
	 * @exception ClassNotFoundEcxeption si une classe particuliere est introuvable
	 * @exception SQLExeption si il y a une erreur dans la requte SQL
	 */
	 private Connection getConnexion()throws ClassNotFoundException, SQLException{
		 System.out.println("modification realises: ");
	        Connection conn = null ;
	        String driver = "com.mysql.cj.jdbc.Driver"; //TODO  complter
	        String url = "jdbc:mysql://sio-hautil.eu:3306/harroy?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC"; //TODO  complter
	        String user="harroy"; //TODO  complter
	        String pwd = "azertyuiop971"; //TODO  complter
	        
	            Class.forName(driver);
	            System.out.println("driver ok");
	            conn=	DriverManager.getConnection(url,user,pwd);
	            System.out.println("connection ok");

	        
	        return conn;
	    }
	 /**
	  * methode qui recupere tous les joueurs de la base de donnes qui prend rien a parametre
	  * @return la liste de joueurs listeJ
	  * @exception DAOException eleve une exeption personnalis� pour les erreurs SQL et de classe particuliere non trouv�
	  * @exception 	ParseExeption si il y a une erreur de format concernant un String 
	  */
	
	ArrayList<JoueurFoot>recupererAllJoueurs()throws  DaoException, ParseException{
		
		 ArrayList<JoueurFoot> listJ = new ArrayList();
		 Connection conn = null;
		 try{
			 conn = getConnexion();
				
				//affiche le contenue de la table avec SELECT
				Statement stmt = conn.createStatement();
				String req2= "SELECT* FROM joueurFoot";
				ResultSet result = stmt.executeQuery(req2);
		
				// On affiche les donnes des diffrentes colonnes
				while(result.next()){
					JoueurFoot j = new JoueurFoot(result.getInt(1),result.getString(2),result.getString(3),result.getString(4),result.getInt(5),result.getString(6),result.getDate(7));
					listJ.add(j);
					}
				 
				
		 } catch (ClassNotFoundException e) {
			 DaoException ex = new DaoException ("il n'y a pas de driver",e);
			 throw ex;
		 	} 
		 catch (SQLException e) {
	        	DaoException ex = new DaoException ("erreur dans la requete SQL", e);
	        	throw ex;
	        	}
		 finally{
			    try{
			        if (conn!=null){
			            conn.close();
			        }
			    } catch (SQLException e){
			        e.printStackTrace();
			    }
			}
		 return listJ;
	}
	/**
	 * methode qui ajoute un joueur dans la base de donn�es
	 * @param j un joueur de type Joueur
	 * @exception DAOException eleve une exeption personnalis� pour les erreurs SQL et de classe particuliere non trouv�
	 * @exception 	ParseExeption si il y a une erreur de format concernant un String
	 * @exception ClassNotFoundEcxeption si une classe particuliere est introuvable
	 * @exception SQLExeption si il y a une erreur dans la requte SQL 
	 */
	 public void insertJoueur(JoueurFoot j) throws  DaoException, ParseException
	    {
	        Connection conn = null;
	        // prparer une requte d'insertion
	        // remplir la requte avec les donnes de l'objet pass en paramtre (p)
	        // excuter la requte
	    	
		 try{
				 conn = getConnexion();
				
				String dateD = "19991025";
				Date dateDeNaissence = new SimpleDateFormat("ddMMyyyy").parse(dateD);
				java.sql.Date dateSQL = new java.sql.Date(dateDeNaissence.getTime());
				
				//cration de la requete prpare
				String req3 = "INSERT INTO joueurFoot VALUES(?,?,?,?,?,?,?)";
				PreparedStatement pstmt = conn.prepareStatement(req3);
				
				//prparation de la requete
			
				
				pstmt.setInt(1,j.getNumLicence());
				pstmt.setString(2, j.getNom());
				pstmt.setString(3, j.getPrenom());
				pstmt.setString(4,j.getPoste());
				pstmt.setInt(5, j.getNum());
				pstmt.setString(6, j.getClub());
				pstmt.setDate(7, dateSQL);
				
				
				//execution de la requete prpare de mise a jour  (DELETET,INSERT,UDATE)
				
				int res2 = pstmt.executeUpdate();
				System.out.println("modification realises: "+res2);
				
				
		 }
		 catch (ClassNotFoundException e) {
			 DaoException ex = new DaoException ("il n'y a pas de driver",e);
			 throw ex;
		 	} 
		 catch (SQLException e) {
	        	DaoException ex = new DaoException ("erreur dans la requete SQL", e);
	        	throw ex;
	        	}
		 
		 finally{
			    try{
			        if (conn!=null){
			            conn.close();
			        }
			    } catch (SQLException e){
			        e.printStackTrace();
			    }
			}
		 
		 
		
	    	}
	 /**
	  * methode qui permet de supprimer un joueur
	  * @param j un joueur de type Joueur
	  * @exception DAOException eleve une exeption personnalis� pour les erreurs SQL et de classe particuliere non trouv�
	  * @exception ClassNotFoundEcxeption si une classe particuliere est introuvable
	  * @exception SQLExeption si il y a une erreur dans la requte SQL
	  */
	 public void deleteJoueur(JoueurFoot j)throws  DaoException
	
	    {
		 Connection conn = null;
	        // rcuprer la connexion (mthode prive de cette classe)
	        // prparer une requte de suppression
	        // remplir la requte avec l'id de l'objet pass en paramtre (p)
	        // excuter la requte
		 try{
				
			 	
			 	conn = getConnexion();
				//cration de la requete prpare
				String req = "DELETE FROM joueurFoot WHERE numlicence = ?;";
				PreparedStatement pstmt = conn.prepareStatement(req);
				
				//preparation de la requete
				pstmt.setInt(1,j.getNumLicence());
				
				
				//execution de la requete prpare de mise a jour  (DELETET,INSERT,UDATE)
				
				int res = pstmt.executeUpdate();
				System.out.println(res+" joueur supp ");
				
				
			
				} catch (ClassNotFoundException e) {
					 DaoException ex = new DaoException ("il n'y a pas de driver",e);
					 throw ex;
				 	} 
				 catch (SQLException e) {
			        	DaoException ex = new DaoException ("erreur dans la requete SQL", e);
			        	throw ex;
			        	}
				 finally{
					    try{
					        if (conn!=null){
					            conn.close();
					        }
					    } catch (SQLException e){
					        e.printStackTrace();
					    }
					}
		 
	    }
	 /**
	  * methode qui affiche les joueurs de la base de donn�es retournant aucune valeur et prenant aucun parametre
	  * @exception DAOException eleve une exeption personnalis� pour les erreurs SQL et de classe particuliere non trouv�
	  * @exception ClassNotFoundEcxeption si une classe particuliere est introuvable
	  * @exception SQLExeption si il y a une erreur dans la requte SQL

	  */
	public void afficherJoueurs() throws  DaoException {
		 Connection conn = null;
		try{
			conn = getConnexion();
			
			//affiche le contenue de la table avec SELECT
			Statement stmt = conn.createStatement();
			String req= "SELECT* FROM joueurFoot";
			ResultSet result = stmt.executeQuery(req);
			
			// On affiche les donnes des diffrentes colonnes
			while(result.next()){
			System.out.println( "id: "+ result.getInt(1));	
			System.out.println( "nom : "+ result.getString(2));// la premiere colonne 
			System.out.println( "prenom "+ result.getString(3));
			System.out.println( "poste: "+ result.getString(4));	
			System.out.println( "numero : "+ result.getInt(5));// la premiere colonne 
			System.out.println( "club : "+ result.getString(6));
			System.out.println( "date de naissance : "+ result.getDate(7));
			
			
			}
			result.close();
			stmt.close();
			
			}catch (ClassNotFoundException e) {
				 DaoException ex = new DaoException ("il n'y a pas de driver",e);
				 throw ex;
			 	} 
			 catch (SQLException e) {
		        	DaoException ex = new DaoException ("erreur dans la requete SQL", e);
		        	throw ex;
		        	}
			 finally{
				    try{
				        if (conn!=null){
				            conn.close();
				        }
				    } catch (SQLException e){
				        e.printStackTrace();
				    }
				}
		}
	
	
	
	/**
	 * methode qui permet de recuperer un joueur selon son numero de licence
	 * @param id un entier, le numero de licence
	 * @return un joueur de type Joueur
	 * @exception DAOException eleve une exeption personnalis� pour les erreurs SQL et de classe particuliere non trouv�
	 * @exception ClassNotFoundEcxeption si une classe particuliere est introuvable
	 * @exception SQLExeption si il y a une erreur dans la requte SQL
	 */
	 public JoueurFoot getJoueurById(String id) throws  DaoException{
		 JoueurFoot j  = null;
		 Connection conn = null;
		 
		 
		 
		 try{
			 
			 conn = getConnexion();
				
				//cration de la requete prpare
				String req = "SELECT* FROM joueurFoot where numLicence = ?";
				PreparedStatement pstmt = conn.prepareStatement(req);
				//preparation de la requete
				pstmt.setString(1,id);
				//execution de la requete
				ResultSet result = pstmt.executeQuery();
				
				while(result.next()){
	                j = new JoueurFoot();
	                j.setNumLicence(result.getInt(1));
	                j.setNom(result.getString(2));
	                j.setPrenom(result.getString(3));
	                j.setPoste(result.getString(4));
	                j.setNum(result.getInt(5));
	                j.setClub(result.getString(6));
	                j.setDateDeNaissance(result.getDate(7));
	               
	                }
	               
					
						
		 }catch (ClassNotFoundException e) {
			 DaoException ex = new DaoException ("il n'y a pas de driver",e);
			 throw ex;
		 	} 
		 catch (SQLException e) {
	        	DaoException ex = new DaoException ("erreur dans la requete SQL", e);
	        	throw ex;
	        	}
		 finally{
			    try{
			        if (conn!=null){
			            conn.close();
			        }
			    } catch (SQLException e){
			        e.printStackTrace();
			    }
			}
		 return j;
	 }
	 /**
		 * methode qui permet de recuperer un joueur selon son numero de licence
		 * @param nom une chaine
		 * @return un joueur de type Joueur
		 * @exception DAOException eleve une exeption personnalis� pour les erreurs SQL et de classe particuliere non trouv�
		 * @exception ClassNotFoundEcxeption si une classe particuliere est introuvable
	     * @exception SQLExeption si il y a une erreur dans la requte SQL
		 */
	 
	 public JoueurFoot getJoueurByName(String nom) throws  DaoException, ParseException{
		 JoueurFoot j  = null;
		 Connection conn = null;
		 try{
			 
			 conn = getConnexion();
				
				//cration de la requete prpare
				String req = "SELECT* FROM joueurFoot WHERE numLicence = ?";
				PreparedStatement pstmt = conn.prepareStatement(req);
				//preparation de la requete
				pstmt.setString(1,nom);
				//execution de la requete
				ResultSet result = pstmt.executeQuery();
				
				while(result.next()){
	                j = new JoueurFoot();
	                j.setNumLicence(result.getInt(1));
	                j.setNom(result.getString(2));
	                j.setPrenom(result.getString(3));
	                j.setPoste(result.getString(4));
	                j.setNum(result.getInt(5));
	                j.setClub(result.getString(6));
	                j.setDateDeNaissance(result.getDate(7));
	                }
					
						
		 }catch (ClassNotFoundException e) {
			 DaoException ex = new DaoException ("il n'y a pas de driver",e);
			 throw ex;
		 	} 
		 catch (SQLException e) {
	        	DaoException ex = new DaoException ("erreur dans la requete SQL", e);
	        	throw ex;
	        	}
		 finally{
			    try{
			        if (conn!=null){
			            conn.close();
			        }
			    } catch (SQLException e){
			        e.printStackTrace();
			    }
			}
		
		 return j;
	 }

}
