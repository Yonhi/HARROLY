package mission1;
import mission1.JoueurFoot;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Date;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;


/**
 * 
 * @author yonhi
 * @version 2.0
 */

public class JoueurDAO {
	
	/**
	 * methode qui permet de se connecter a la base de donn�es qui prend rien en parametre
	 * @return conn
	 * 
	 */
	 private Connection getConnexion(){
		 System.out.println("modification realis�es: ");
	        Connection conn = null ;
	        String driver = "com.mysql.cj.jdbc.Driver"; //TODO � compl�ter
	        String url = "jdbc:mysql://sio-hautil.eu:3306/harroy?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC"; //TODO � compl�ter
	        String user="harroy"; //TODO � compl�ter
	        String pwd = "azertyuiop971"; //TODO � compl�ter
	        try{
	            Class.forName(driver);
	            System.out.println("driver ok");
	            conn=	DriverManager.getConnection(url,user,pwd);
	            System.out.println("connection ok");

	        }catch(Exception e){
	            e.printStackTrace();
	        }
	        return conn;
	    }
	
	 /**
	  * methode qui recupere tous les joueurs de la base de donnes qui prend rien a parametre
	  * @return la liste de joueurs listeJ
	  */
	ArrayList<JoueurFoot>recupererAllJoueurs(){
		
		 ArrayList<JoueurFoot> listJ = new ArrayList();
		 try{
			 Connection conn = getConnexion();
				
				//affiche le contenue de la table avec SELECT
				Statement stmt = conn.createStatement();
				String req2= "SELECT* FROM joueurFoot2";
				ResultSet result = stmt.executeQuery(req2);
		
				// On affiche les donne�s des diff�rentes colonnes
				while(result.next()){
					JoueurFoot j = new JoueurFoot(result.getInt(1),result.getString(2),result.getString(3),result.getString(4),result.getInt(5),result.getString(6),result.getDate(7));
					listJ.add(j);
					}
				 
				
		 }catch(Exception e){
				e.printStackTrace();
				}
		 return listJ;
	}
	/**
	 * methode qui ajoute un joueur dans la base de donn�es
	 * @param j un joueur de type Joueur
	 */
	 public void insertJoueur(JoueurFoot j) 
	    {
	        
	        // pr�parer une requ�te d'insertion
	        // remplir la requ�te avec les donn�es de l'objet pass� en param�tre (p)
	        // ex�cuter la requ�te
	    	
		 try{
				Connection conn = getConnexion();
				
				String dateD = "19991025";
				Date dateDeNaissence = new SimpleDateFormat("ddMMyyyy").parse(dateD);
				java.sql.Date dateSQL = new java.sql.Date(dateDeNaissence.getTime());
				
				//cr�ation de la requete pr�par�e
				String req3 = "INSERT INTO joueurFoot2 VALUES(?,?,?,?,?,?,?)";
				PreparedStatement pstmt = conn.prepareStatement(req3);
				
				//pr�paration de la requete
			
				
				pstmt.setInt(1,j.getNumLicence());
				pstmt.setString(2, j.getNom());
				pstmt.setString(3, j.getPrenom());
				pstmt.setString(4,j.getPoste());
				pstmt.setInt(5, j.getNum());
				pstmt.setString(6, j.getClub());
				pstmt.setDate(7, dateSQL);
				
				
				//execution de la requete pr�par�e de mise a jour  (DELETET,INSERT,UDATE)
				
				int res2 = pstmt.executeUpdate();
				System.out.println("modification realis�es: "+res2);
				
				
		 }catch(Exception e){
				e.printStackTrace();
				}
	    	}
	 /**
	  * methode qui permet de supprimer un joueur
	  * @param j un joueur de type Joueur
	  */
	 public void deleteJoueur(JoueurFoot j)
	
	    {
	        // r�cup�rer la connexion (m�thode priv�e de cette classe)
	        // pr�parer une requ�te de suppression
	        // remplir la requ�te avec l'id de l'objet pass� en param�tre (p)
	        // ex�cuter la requ�te
		 try{
				
			 	
			 	Connection conn = getConnexion();
				//cr�ation de la requete pr�par�e
				String req = "DELETE FROM joueurFoot2 WHERE numlicence = ?;";
				PreparedStatement pstmt = conn.prepareStatement(req);
				
				//preparation de la requete
				pstmt.setInt(1,j.getNumLicence());
				
				
				//execution de la requete pr�par�e de mise a jour  (DELETET,INSERT,UDATE)
				
				int res = pstmt.executeUpdate();
				System.out.println(res+" joueur supp ");
				
				
			
				}catch(Exception e){
				e.printStackTrace();
				}
		 
	    }


	 /**
	  * methode qui affiche les joueurs de la base de donn�es retournant aucune valeur et prenant aucun parametre
	  * 
	  */
	public void afficherJoueurs() {
		try{
			Connection conn = getConnexion();
			
			//affiche le contenue de la table avec SELECT
			Statement stmt = conn.createStatement();
			String req= "SELECT* FROM joueurFoot2";
			ResultSet result = stmt.executeQuery(req);
			
			// On affiche les donne�s des diff�rentes colonnes
			while(result.next()){
			System.out.println( "id: "+ result.getInt(1));	
			System.out.println( "nom : "+ result.getString(2));// la premiere colonne 
			System.out.println( "prenom "+ result.getString(3));
			System.out.println( "poste: "+ result.getString(4));	
			System.out.println( "numero : "+ result.getInt(5));// la premiere colonne 
			System.out.println( "club : "+ result.getString(6));
			System.out.println( "date de naissance : "+ result.getDate(7));
			
			
			}
			result.close();
			stmt.close();
			
			}catch(Exception e){
			e.printStackTrace();
			}
		}
	
	/**
	 * methode qui permet de recuperer un joueur selon son numero de licence
	 * @param id un entier, le numero de licence
	 * @return un joueur de type Joueur
	 */
	 public JoueurFoot getJoueurById(int id) {
		 JoueurFoot j  = null;
		 
		 try{
			 
				Class.forName("com.mysql.cj.jdbc.Driver");
				System.out.println("driver ok");
				Connection conn=
				DriverManager.getConnection("jdbc:mysql://sio-hautil.eu:3306/harroy?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC" +
		                "&user=harroy&password=azertyuiop971");
				System.out.println("connection ok");
				
				//cr�ation de la requete pr�par�e
				String req = "SELECT* FROM joueurFoot2 where numLicence = ?";
				PreparedStatement pstmt = conn.prepareStatement(req);
				//preparation de la requete
				pstmt.setInt(1,id);
				//execution de la requete
				ResultSet result = pstmt.executeQuery();
				
				while(result.next()){
	                j = new JoueurFoot();
	                j.setNumLicence(result.getInt(1));
	                j.setNom(result.getString(2));
	                j.setPrenom(result.getString(3));
	                j.setPoste(result.getString(4));
	                j.setNum(result.getInt(5));
	                j.setClub(result.getString(6));
	                j.setDateDeNaissance(result.getDate(7));
	                }
	               
					
						
		 }catch(Exception e){
				e.printStackTrace();
				}
		
		 return j;
	 }
	
	 /**
		 * methode qui permet de recuperer un joueur selon son numero de licence
		 * @param nom une chaine
		 * @return un joueur de type Joueur
		 */
	 
	 public JoueurFoot getJoueurByName(String nom) {
		 JoueurFoot j  = null;
		 try{
			 
			 Connection conn = getConnexion();
				
				//cr�ation de la requete pr�par�e
				String req = "SELECT* FROM joueurFoot2 WHERE numLicence = ?";
				PreparedStatement pstmt = conn.prepareStatement(req);
				//preparation de la requete
				pstmt.setString(1,nom);
				//execution de la requete
				ResultSet result = pstmt.executeQuery();
				
				while(result.next()){
	                j = new JoueurFoot();
	                j.setNumLicence(result.getInt(1));
	                j.setNom(result.getString(2));
	                j.setPrenom(result.getString(3));
	                j.setPoste(result.getString(4));
	                j.setNum(result.getInt(5));
	                j.setClub(result.getString(6));
	                j.setDateDeNaissance(result.getDate(7));
	                }
					
						
		 }catch(Exception e){
				e.printStackTrace();
				}
		
		 return j;
	 }

}
