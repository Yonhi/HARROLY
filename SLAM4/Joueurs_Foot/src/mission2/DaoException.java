package mission2;

import java.sql.*;
import java.util.ArrayList;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * 
 * @author yonhi
 * version1.0
 */

public class DaoException extends Exception {

    private String msg;

/**
 * constructeur qui permet de cree un objet qui permet d'informer l'erreur et sa cause
 * un constructeur avec plusieur parametre
 * @param msg
 * @param t
 */
    public DaoException(String msg,Throwable t) {
        super(msg,t);
        
    }
  
   
}
