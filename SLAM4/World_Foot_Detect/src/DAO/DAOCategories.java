package DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


import classe.Categorie;




public class DAOCategories {
	Categorie cat ;

	/**
	 * methode qui permet de se connecter a la base de donn�es qui prend rien en parametre
	 * @return conn
	 * 
	 */
	 private Connection getConnexion(){
		 System.out.println("modification realis�es: ");
	        Connection conn = null ;
	        String driver = "com.mysql.cj.jdbc.Driver"; //TODO � compl�ter
	        String url = "jdbc:mysql://sl-us-south-1-portal.38.dblayer.com:58951/ E4_Worl_Foot_Detect"; //TODO � compl�ter
	        String user="admin"; //TODO � compl�ter
	        String pwd = "FLYCWHAXQMPCHRVM"; //TODO � compl�ter
	        try{
	            Class.forName(driver);
	            System.out.println("driver ok");
	            conn=	DriverManager.getConnection(url,user,pwd);
	            System.out.println("connection ok");

	        }catch(Exception e){
	            e.printStackTrace();
	        }
	        return conn;
	    }
	 
	 
	 public void insertCategorie(Categorie cat) 
	    {
		 Connection conn = null;
	        
	        // pr�parer une requ�te d'insertion
	        // remplir la requ�te avec les donn�es de l'objet pass� en param�tre (p)
	        // ex�cuter la requ�te
	    	
		 try{
			 conn = getConnexion();
				
				//cr�ation de la requete pr�par�e
				String req = "INSERT INTO categories VALUES(?,?)";
				PreparedStatement pstmt = conn.prepareStatement(req);
				
				//pr�paration de la requete
				pstmt.setString(1,cat.getId());
				pstmt.setString(2, cat.getLibelle());
				
				//execution de la requete pr�par�e de mise a jour  (DELETET,INSERT,UDATE)
				int res2 = pstmt.executeUpdate();
				System.out.println("modification realis�es: "+res2);
				
				
		 }catch(Exception e){
				e.printStackTrace();
				}
		 finally{
			    try{
			        if (conn!=null){
			            conn.close();
			        }
			    } catch (SQLException e){
			        e.printStackTrace();
			    }
			}
	    	}
	 
	 public void deleteCategorie(Categorie delCat)
	    {
		 Connection conn = null;
	        // rcuprer la connexion (mthode prive de cette classe)
	        // prparer une requte de suppression
	        // remplir la requte avec l'id de l'objet pass en paramtre (p)
	        // excuter la requte
		 try{
				
			 	
			 	conn = getConnexion();
				//cration de la requete prpare
				String req = "DELETE FROM categories WHERE id = ?;";
				PreparedStatement pstmt = conn.prepareStatement(req);
				
				//preparation de la requete
				pstmt.setString(1,delCat.getId());
				
				
				//execution de la requete prpare de mise a jour  (DELETET,INSERT,UDATE)
				
				int res = pstmt.executeUpdate();
				System.out.println(res+" joueur supp ");
				
				
			
				}  
		 catch(Exception e){
				e.printStackTrace();
				}
				 finally{
					    try{
					        if (conn!=null){
					            conn.close();
					        }
					    } catch (SQLException e){
					        e.printStackTrace();
					    }
					}
		 
	    }
	 public Categorie getCategorie() {
			return this.cat;
		}
	 
	 
	 public Categorie getJoueurByName(String nom) {
		 Categorie cat  = null;
		 Connection conn = null;
		 try{
			 
			 conn = getConnexion();
				
				//cration de la requete prpare
				String req = "SELECT* FROM categories WHERE id = ?";
				PreparedStatement pstmt = conn.prepareStatement(req);
				//preparation de la requete
				pstmt.setString(1,nom);
				//execution de la requete
				ResultSet result = pstmt.executeQuery();
				
				while(result.next()){
	                cat = new Categorie();
	                cat.setId(result.getString(1));
	                cat.setLibelle(result.getString(2));
	            
	                }
					
						
		 }
		 catch(Exception e){
				e.printStackTrace();
				}
		 finally{
			    try{
			        if (conn!=null){
			            conn.close();
			        }
			    } catch (SQLException e){
			        e.printStackTrace();
			    }
			}
		
		 return cat;
	 }
	 
	 public void afficherCategories()  {
		 Connection conn = null;
		try{
			conn = getConnexion();
			
			//affiche le contenue de la table avec SELECT
			Statement stmt = conn.createStatement();
			String req= "SELECT* FROM categories";
			ResultSet result = stmt.executeQuery(req);
			
			// On affiche les donnes des diffrentes colonnes
			while(result.next()){
			System.out.print( "id: "+ result.getString(1));	
			System.out.println( "nom : "+ result.getString(2));
			
			
			
			}
			result.close();
			stmt.close();
			
			} catch(Exception e){
				e.printStackTrace();
				}
			 finally{
				    try{
				        if (conn!=null){
				            conn.close();
				        }
				    } catch (SQLException e){
				        e.printStackTrace();
				    }
				}
		}
	 

}
