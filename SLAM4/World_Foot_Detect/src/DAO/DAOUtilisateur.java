package DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import classe.Categorie;
import classe.Joueurs;
import classe.Utilisateur;
import classe.Bulletin;

public class DAOUtilisateur {
	
	/**
	 * methode qui permet de se connecter a la base de donn�es qui prend rien en parametre
	 * @return conn
	 * 
	 */
	 private Connection getConnexion(){
		 System.out.println("modification realis�es: ");
	        Connection conn = null ;
	        String driver = "com.mysql.cj.jdbc.Driver"; //TODO � compl�ter
	        String url = "jdbc:mysql://sl-us-south-1-portal.38.dblayer.com:58951/E4_Worl_Foot_Detect"; //TODO � compl�ter
	        String user="admin"; //TODO � compl�ter
	        String pwd = "FLYCWHAXQMPCHRVM"; //TODO � compl�ter
	        try{
	            Class.forName(driver);
	            System.out.println("driver ok");
	            conn=	DriverManager.getConnection(url,user,pwd);
	            System.out.println("connection ok");

	        }catch(Exception e){
	            e.printStackTrace();
	        }
	        return conn;
	    }
	 
	 
	 public void insertUtilisateur(Utilisateur utl ) 
	    {
		 Connection conn = null;
	        
	        // pr�parer une requ�te d'insertion
	        // remplir la requ�te avec les donn�es de l'objet pass� en param�tre (p)
	        // ex�cuter la requ�te
	    	
		 try{
			 conn = getConnexion();
				
				//cr�ation de la requete pr�par�e
				String req = "INSERT INTO utilisateur VALUES(?,?,?,?,?)";
				PreparedStatement pstmt = conn.prepareStatement(req);
				
				//pr�paration de la requete
				pstmt.setString(1,utl.getId());
				pstmt.setString(2, utl.getLogin());
				pstmt.setString(3, utl.getEmail());
				pstmt.setString(4, utl.getMdp());
				pstmt.setString(5, utl.getType());
			
				
				
				//execution de la requete pr�par�e de mise a jour  (DELETET,INSERT,UDATE)
				int res2 = pstmt.executeUpdate();
				System.out.println("modification realis�es: "+res2);
				
				
		 }catch(Exception e){
				e.printStackTrace();
				}
		 finally{
			    try{
			        if (conn!=null){
			            conn.close();
			        }
			    } catch (SQLException e){
			        e.printStackTrace();
			    }
			}
	    	}
	 public Utilisateur getUserByName(String nom) {
		 Utilisateur utl  = null;
		 Connection conn = null;
		 try{
			 
			 conn = getConnexion();
				
				//cration de la requete prpare
				String req = "SELECT* FROM utilisateur WHERE id = ?";
				PreparedStatement pstmt = conn.prepareStatement(req);
				//preparation de la requete
				pstmt.setString(1,nom);
				//execution de la requete
				ResultSet result = pstmt.executeQuery();
				
				while(result.next()){
	                utl= new Utilisateur();
	                utl.setId(result.getString(1));
	                utl.setLogin(result.getString(2));
	                utl.setEmail(result.getString(3));
	                utl.setMdp(result.getString(4));
	                utl.setType(result.getString(5));
	            
	                }
					
						
		 }
		 catch(Exception e){
				e.printStackTrace();
				}
		 finally{
			    try{
			        if (conn!=null){
			            conn.close();
			        }
			    } catch (SQLException e){
			        e.printStackTrace();
			    }
			}
		
		 return utl;
	 }

}



