package classe;
import java.util.Date;  
public class MatchDetection {
	String id ;
	Categorie categorie ;
	Date dateMatch ; 
	
	public MatchDetection (String id,Categorie categorie, Date dateMatch ) {

		this.id = id ;
		this.categorie = categorie;
		this.dateMatch = dateMatch;
		
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Categorie getCategorie() {
		return categorie;
	}

	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}

	public Date getDateMatch() {
		return dateMatch;
	}

	public void setDateMatch(Date dateMatch) {
		this.dateMatch = dateMatch;
	}
	public String toString () { 
		return  " club : "+ this.dateMatch+ categorie.toString(); 
		}

}
