package classe;

public class Categorie {
	
		public String id;
		public  String libelle;
		

		public Categorie(String id,String libelle) {
			this.id = id;
			this.libelle = libelle;
		}
		public Categorie() {
			
		}


		public String getId() {
			return id;
		}


		public void setId(String id) {
			this.id = id;
		}


		public String getLibelle() {
			return libelle;
		}
		
		public void setLibelle(String libelle) {
			this.libelle = libelle;
		}


		
		
		@Override
		public String toString() {
			return "Categorie [id=" + id + ", libelle=" + libelle + "]";
		}
		

	}
