package classe;

import classe.Utilisateur;
import classe.Categorie;
public class Joueurs extends Utilisateur {
	String id ;  
	String nom ;
	String prenom ;
	String poste ;
	String club ;
	Categorie categorie;
	String poids;
	int nbButs ;
	int nbPasseD ;

	
	
	
	public Joueurs (String id,String login,String email,String type ,String mdp,String categorieU ,String nom,String prenom,String poste,String club, Categorie categorie,String poids,int nButs,int nbPasseD) {
		super (id,login,email,mdp,type);
		this.id = id ;
		this.nom = nom ;
		this.prenom = prenom;
		this.club = club;
		this.categorie = categorie;
		this.poids = poids;
		this.nbButs = nbButs;
		this.nbPasseD = nbPasseD;
		
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getPoste() {
		return poste;
	}

	public void setPoste(String poste) {
		this.poste = poste;
	}

	public String getClub() {
		return club;
	}

	public void setClub(String club) {
		this.club = club;
	}

	public Categorie getCategorie() {
		return categorie;
	}

	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}

	public String getPoids() {
		return poids;
	}

	public void setPoids(String poids) {
		this.poids = poids;
	}

	public int getNbButs() {
		return nbButs;
	}

	public void setNbButs(int nbButs) {
		this.nbButs = nbButs;
	}

	public int getNbPasseD() {
		return nbPasseD;
	}

	public void setNbPasseD(int nbPasseD) {
		this.nbPasseD = nbPasseD;
	}

	
	
	
	

	public String toString () { 
		return super.toString()+ " Nom : "+ this.nom ; 
		}
	
	

}
