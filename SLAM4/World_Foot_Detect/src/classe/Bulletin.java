package classe;

public class Bulletin {
	String id ;
	double francais ;
	double maths ;
	double histGeo ;
	double anglais;
	int trimestre;


	public Bulletin (String id,double francais,double maths, double histGeo,double anglais,int trimestre) {

		this.id = id ;
		this.francais = francais ;
		this.maths = maths;
		this.histGeo = histGeo;
		this.anglais = anglais;
		this.trimestre = trimestre;

	}



	
	
	public String getId() {
		return id;
	}





	public void setId(String id) {
		this.id = id;
	}





	public int getTrimestre() {
		return trimestre;
	}





	public void setTrimestre(int trimestre) {
		this.trimestre = trimestre;
	}





	public double getFrancais() {
		return francais;
	}





	public void setFrancais(double francais) {
		this.francais = francais;
	}





	public double getMaths() {
		return maths;
	}





	public void setMaths(double maths) {
		this.maths = maths;
	}





	public double getHistGeo() {
		return histGeo;
	}





	public void setHistGeo(double histGeo) {
		this.histGeo = histGeo;
	}





	public double getAnglais() {
		return anglais;
	}





	public void setAnglais(double anglais) {
		this.anglais = anglais;
	}





	public String toString () { 
		return "francais: " + this.francais; 
		}
}