 package classe;

public class Recruteur {
	String id ;
	String club;
	Categorie categorie ;
	
	public Recruteur (String id,String club,Categorie categorie) {

		this.id = id ;
		this.club = club ;
		this.categorie = categorie;
		
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getClub() {
		return club;
	}

	public void setClub(String club) {
		this.club = club;
	}

	public Categorie getCategorie() {
		return categorie;
	}

	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}
	public String toString () { 
		return super.toString()+ " club : "+ this.club + categorie.toString(); 
		}

}
