package application;
	


import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;




public class Main extends Application {

public static void main(String[] args) {//methode qui appelle la fontion launch qui lance le reste du programme
    Application.launch(Main.class, args);
}

@Override
public void start (Stage primaryStage) { //methode qui appelle la fonction launch() qui prend un objet de type stage (le theatre de l'application)
	primaryStage.setTitle("MAJ-MIN");
	/*
	 * creation du layout sous forme de lignes et collones
	 */
	GridPane grid = new GridPane();
	grid.setAlignment(Pos.CENTER);
	grid.setHgap(10);
	grid.setVgap(10);
	grid.setPadding(new Insets(25, 25, 25, 25));
	
	/*
	 * creation du formulaire
	 */
	Text scenetitle = new Text("Majuscule ou minuscule ");
	scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
	grid.add(scenetitle, 0, 0, 2, 1);

	Label userName = new Label("entrez un texte:");
	grid.add(userName, 0, 1);

	TextField userTextField = new TextField();
	grid.add(userTextField, 1, 1);
	
	

	
	
	/*
	 * ajout d'un bouton
	 */

	
	Button btnMaj = new Button("Majuscule");
	HBox hbBtnMaj = new HBox (10);
	hbBtnMaj.getChildren (). add (btnMaj);
	grid.add (hbBtnMaj, 1, 3);
	
	Button btnMin = new Button("Minsucule");
	HBox hbBtnMin = new HBox (1);
	hbBtnMaj.getChildren (). add (btnMin);
	grid.add (hbBtnMin, 1, 10);
	
	final Text actiontarget = new Text();
    grid.add(actiontarget, 1, 4);
   
    //gere des evenement
    btnMaj.setOnAction(new EventHandler<ActionEvent>() {
    	 
        @Override
        public void handle(ActionEvent e) {
            actiontarget.setFill(Color.BLACK);
            userTextField.setText(userTextField.getText().toUpperCase());
            actiontarget.setText(userTextField.getText().toUpperCase());
            userTextField.clear();

        }
        
        
        
    });
    btnMin.setOnAction(new EventHandler<ActionEvent>() {
   	 
        @Override
        public void handle(ActionEvent e) {
            actiontarget.setFill(Color.FIREBRICK);
            userTextField.setText(userTextField.getText().toLowerCase());
            actiontarget.setText(userTextField.getText().toLowerCase());
            userTextField.clear();

        }
        
        
        
    });


	/*
	 * on cree la scene qui conien le GridPane qui contiendra les objets
	 * on ajoute lka scene a l'objet stage qui coresspond au theatre
	 */
	Scene scene = new Scene(grid, 300, 275);
	primaryStage.setScene(scene);
	
	primaryStage.show ();
    
}
}

