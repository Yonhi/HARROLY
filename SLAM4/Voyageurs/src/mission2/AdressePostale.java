package mission2;

public class AdressePostale {
	private String libelle;
	private String ville;
	private String codepostale;
	
	AdressePostale(String lib, String vil,String code){
		libelle = lib;
		ville = vil;
		codepostale = code;
	}
	void Afficher() {
		System.out.println( "adresse du voyageurs : " + libelle +" " + ville+ " " +  codepostale);
	}
	
	
	public void setLibelle(String l) {
		this.libelle= l;
	}
	 String getLib() {
		 return this.libelle;
	 }
	 
	
	public void setVille(String V) {
		this.ville=V;
	}
	 String getVil() {
		 return this.ville;
	 }
	 
	 
	public void setCodepostale(String C) {
		this.codepostale=C;
	}
	 String getCodeP() {
		 return this.codepostale;
	 }
	
	

}
