package mission4;

public class main {
	public static void main(String[] args) {
		 Bagage b1 = new Bagage(410,"bleu",43);
		 System.out.println(b1);//affiche le bagage avec la methode toString
		 
		 AdressePostale ads = new AdressePostale("9 mail mendes france","Vaureal","95490");
		 Voyageurs v = new Voyageurs("toto",18,ads,b1);
		 System.out.println(v);//affiche le voyageur avec la methode toString
		 
		 //etape 2 
		 //creation du voyageur handicapé
		 VoyageurHandicape vh = new VoyageurHandicape ("tata",20,ads,b1,"paralysie");
		 System.out.println(vh);
		 
		 //creation du voyageur privilege
		 Bagage b2 = new Bagage(214,"bleu",22);
		 AdressePostale ads2 = new AdressePostale("8 avenue du chene","Paris","75700");
		 VoyageurPrivilege vp = new VoyageurPrivilege("tutu",30,ads2,b2,945946);
		 System.out.println(vp);
		 
		 
		 
	}
}
