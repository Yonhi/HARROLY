package mission4;

public class AdressePostale {
	private String libelle;
	private String ville;
	private String codePostale;
	
	AdressePostale(String lib, String vil,String code){
		libelle = lib;
		ville = vil;
		codePostale = code;
	}
	AdressePostale(){
		libelle = "nulle";
		ville = "nulle";
		codePostale = "nulle";
	}
	
	void Afficher() {
		System.out.println( "adresse : " + libelle +" " + ville+ " " +  codePostale);
	}
	
	
	public void setLibelle(String l) {
		this.libelle= l;
	}
	 String getLib() {
		 return this.libelle;
	 }
	 
	
	public void setVille(String V) {
		this.ville=V;
	}
	 String getVille() {
		 return this.ville;
	 }
	 
	 
	public void setCodepostale(String C) {
		this.codePostale=C;
	}
	 String getCodeP() {
		 return this.codePostale;
	 }
	
	 public String toString () { 
		 return "Adresse: "+this.libelle+" "+this.ville+" "+ this.codePostale;  
			}

}
