package mission4;

public class VoyageurPrivilege extends Voyageurs{
	
	int codePrivilege;
	
	public VoyageurPrivilege (String nom,int age, AdressePostale adresse,Bagage bag, int codePrivilege) {
		super (nom,age,adresse,bag);
		this.codePrivilege = codePrivilege;
	}
	public String toString () { 
		return super.toString()+ " code privilege :  "+ this.codePrivilege; 
		}
	
	public int getCodePrivilege() {
		return this.codePrivilege;
	}

	public void setCodePrivileges(String typeHdcp) {
		this.codePrivilege = codePrivilege;
	}

}
