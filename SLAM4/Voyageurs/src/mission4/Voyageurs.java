package mission4;



public class Voyageurs {
	protected String nom;
	protected int age;
	protected AdressePostale adresse;
	protected Bagage bag;
	Voyageurs(String N, int A) {
		nom = N;
		age = A;


	}
	Voyageurs() {
		nom = " ";
		age = 0 ; 
	}
	Voyageurs(String n, int a,AdressePostale adr,Bagage b){
		nom = n ;
		age = a ;
		adresse = adr ;
		bag = b ;
		
	}
	public void setNom(String N){
		if(N.length() >= 2){
			nom = N;
		}
	}
	public  String getNom() {
		return this.nom;
	}
	public void setAge(int A){
		if(A>0){
			this.age = A;
		}
	}
	public  int getAge() {
		return this.age;
	}

	public void setAdresse(AdressePostale a){
		this.adresse = a;
		
	}
	AdressePostale getAdresse() {
		return this.adresse;
	}
	
	public void setBag(Bagage b) {
		this.bag = b;
	}
	Bagage getBag() {
		return this.bag;
	}
	
	@Override // annotation pour valider la redéfinition 
	public String toString () { 
		return "Voyageurs:"+this.nom+","+this.age+" ans"+" "+this.adresse.toString()+" "+this.bag.toString(); 
		}
	
	void afficher () {
		System.out.println ("nom du voyageur "+":"+" "+nom);
		System.out.println ("age du voyageur "+":"+" "+age+" "+"ans");
		adresse.Afficher();
		 if (bag != null) {
	            bag.AfficherBag();
	        }
		}

}
